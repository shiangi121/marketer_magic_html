<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Marketer Magic</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <link rel="stylesheet" href="css/ss_style.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->

</head>

<body>

    <div id="dashboard" class="pb-50">
        <div class="dashboard-enterscreen text-center">
            <div class="row">
                <div class="col-md-12">
                    <h5>Hi there ! <img src="images/smiley.svg" class="img-fluid" alt=""> </h5>
                    <p>Welcome to MarketerMagic. The top social proof conversion platform on the planet — let's get started.</p>
                </div>
            </div>
        </div>
        <div class="dash-img mobile-none">
            <img src="images/dash-img.png" class="img-fluid" alt="">
        </div>
        <!-- <select id="sel" onmouseover="DropList()" onmouseout="this.size=1;">
              <option>One</option>
              <option>Two</option>
              <option>Three</option> 
              <option>Four</option> 
              <option>Five</option>
              <option>Six</option>
              <option>Seven</option>
              <option>Eight</option>
          </select> -->
        <div class="dash-quickstart">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-capitalize mb-3">quick start</h5>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="box-shadow text-center">
                        <div class="text-right info">
                            <a href="#"> <i class="fas fa-ellipsis-h"></i></a>
                        </div>
                        <div class="bg-green mb-4">
                            <img src="images/clickproof-green.svg" class="img-fluid" alt="">
                        </div>
                        <div class="content">
                            <h5 class="text-capitalize"><b>click proof</b></h5>
                            <p> <span class="main-color">10,000+ </span> Users saw your ClickProof in the last week!</p>
                        </div>
                        <div class="border-top">
                            <button class="btn launch-btn" onclick="window.location.href='clickproof.php'" type="button">launch now <i class="fas fa-arrow-right"></i> </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="box-shadow text-center">
                        <div class="text-right info">
                            <a href="#"> <i class="fas fa-ellipsis-h"></i></a>
                        </div>
                        <div class="bg-red mb-4">
                            <img src="images/livepic-red.svg" class="img-fluid" alt="">
                        </div>
                        <div class="content">
                            <h5 class="text-capitalize"><b>live pic</b></h5>
                            <p> Lorem Ipsum has been the <span class="main-color">industry's</span> standard dummy </p>
                        </div>
                        <div class="border-top">
                            <button class="btn launch-btn" onclick="window.location.href='livepic.php'" type="button">launch now <i class="fas fa-arrow-right"></i> </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="box-shadow text-center">
                        <div class="text-right info">
                            <a href="#"> <i class="fas fa-ellipsis-h"></i></a>
                        </div>
                        <div class="bg-blue mb-4">
                            <img src="images/hubstaff-blue.svg" class="img-fluid" alt="">
                        </div>
                        <div class="content">
                            <h5 class="text-capitalize"><b>hubstuff</b></h5>
                            <p> Your employees worked <span class="main-color">10,000+</span> hours in the last 28 days!</p>

                        </div>
                        <div class="border-top">
                            <button class="btn launch-btn" onclick="window.location.href='hubstaff.php'" type="button">launch now <i class="fas fa-arrow-right"></i> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</body>

</html>