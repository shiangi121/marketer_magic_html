<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/clickproof.css">

    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5  mt-77" id="clickproofwizard">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h5 class="text-capitalize color-grey font-weight-200">
                            <a href="clickprooflist.php">
                                <svg width="15" height="15" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.57256 0.148624C4.76329 -0.0495412 5.08072 -0.0495412 5.27812 0.148624C5.46885 0.340094 5.46885 0.658764 5.27812 0.849788L1.70054 4.4413H13.5061C13.7813 4.44175 14 4.66134 14 4.93761C14 5.21388 13.7813 5.44061 13.5061 5.44061H1.70054L5.27812 9.02542C5.46885 9.22359 5.46885 9.54271 5.27812 9.73373C5.08072 9.93189 4.76285 9.93189 4.57256 9.73373L0.148047 5.29198C-0.0493488 5.10051 -0.0493488 4.78184 0.148047 4.59082L4.57256 0.148624Z" fill="#9B9B9B"/>
                                </svg>
                                <span class="pl-2" style="vertical-align: middle;">Campaign Sample 01</span>
                            </a>
                        </h5>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <button class="btn save-draft mr-3" type="button">Save as draft</button>
                        <button class="btn linear-btn linear-btn-shadow">next</button>
                    </div>
                </div>
                <div class="row wizard my-5">
                    <div class="col-md-12 px-0">
                        <ul class="nav nav-pills main-wizard" id="pills-tab" role="tablist">
                            <li class="nav-item col-md-4 text-center">
                                <a class="nav-link text-capitalize active" id="pills-url-tab" data-toggle="pill" href="#pills-url" role="tab" aria-controls="pills-url" aria-selected="true">
                                    <span class="number">1</span> url
                                </a>
                            </li>
                            <li class="nav-item col-md-4 text-center wizard-border">
                                <a class="nav-link text-capitalize " id="pills-type-tab" data-toggle="pill" href="#pills-type" role="tab" aria-controls="pills-type" aria-selected="false">
                                    <span class="number">2</span> type
                                </a>
                            </li>
                            <li class="nav-item col-md-4 text-center">
                                <a class="nav-link text-capitalize" id="pills-code-tab" data-toggle="pill" href="#pills-code" role="tab" aria-controls="pills-code" aria-selected="false">
                                    <span class="number">3</span> code
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content main-wizard-content" id="pills-tabContent">
                            <div class="tab-pane fade show active py-5" id="pills-url" role="tabpanel" aria-labelledby="pills-url-tab">
                                <ul class="nav nav-pills mb-3 mt-5 justify-content-center funneltab" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active linear-btn text-uppercase" id="pills-funnel-tab" data-toggle="pill" href="#pills-funnel" role="tab" aria-controls="pills-funnel" aria-selected="true">
                                            <h3 class="text-capitalize font-weight-700 d-inline">Funnel</h3>
                                            <div class="tooltip"><i class="fas fa-info-circle"></i>
                                                <span class="tooltiptext">
                                                    <p class="color-black mb-2">What it does?</p>
                                                    <p class="mb-0">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                                    <button class="btn" type="button">learn more</button>
                                                </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link linear-btn text-uppercase" id="pills-nonfunnel-tab" data-toggle="pill" href="#pills-nonfunnel" role="tab" aria-controls="pills-nonfunnel" aria-selected="false">
                                            <h3 class="text-capitalize font-weight-700 d-inline">Non Funnel</h3>
                                            <div class="tooltip"><i class="fas fa-info-circle"></i>
                                                <span class="tooltiptext">
                                                    <p class="color-black mb-2">What it does?</p>
                                                    <p class="mb-0">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                                    <button class="btn" type="button">learn more</button>
                                                </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content mt-5" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-funnel" role="tabpanel" aria-labelledby="pills-funnel-tab">
                                        <div class="funnel text-center pb-5">
                                            <p class="color-grey my-4">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.</p>
                                            <form action="" class="pt-5">
                                                <div class="link-icon">
                                                    <i class="fas fa-link"></i>
                                                    <input type="text" value="http://sample.com/" data-role="tagsinput" id="tags" class="form-control" placeholder="ClickFunnels">
                                                </div>
                                                <div class="link-icon">
                                                    <i class="fas fa-link"></i>
                                                    <input type="text" value="" data-role="tagsinput" id="tags" class="form-control" placeholder="Website">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-nonfunnel" role="tabpanel" aria-labelledby="pills-nonfunnel-tab">
                                        <div class="non-funnel text-center pb-5">
                                            <p class="color-grey my-4">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.</p>
                                            <form action="" class="pt-5">
                                                <div class="link-icon">
                                                    <i class="fas fa-link"></i>
                                                    <input type="text" value="http://sample.com/" data-role="tagsinput" id="tags" class="form-control" placeholder="Enter Capture URL">
                                                </div>
                                                <div class="link-icon">
                                                    <i class="fas fa-link"></i>
                                                    <input type="text" value="" data-role="tagsinput" id="tags" class="form-control" placeholder="Enter Target URL">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="tab-pane fade " id="pills-type" role="tabpanel" aria-labelledby="pills-type-tab">
                                <div class="notification-style py-5">
                                    <h5 class="font-weight-700 text-center mb-5">Select your notification style</h5>
                                    <div class="row margin-20">
                                        <div class="col-md-12 col-lg-4">
                                            <div class="box-shadow mb-0">
                                                <div class="marked-active" data-toggle="collapse" href="#notification1" role="button" aria-expanded="true" aria-controls="notification1">
                                                    <div class="padding-20">
                                                        <div class="text-left d-inline check-active">
                                                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16 31C24.2843 31 31 24.2843 31 16C31 7.71573 24.2843 1 16 1C7.71573 1 1 7.71573 1 16C1 24.2843 7.71573 31 16 31Z" fill="white" stroke="#EDEDED"/>
                                                                <path fill-rule="evenodd" id="active-fill" clip-rule="evenodd" d="M16 28C22.6274 28 28 22.6274 28 16C28 9.37258 22.6274 4 16 4C9.37258 4 4 9.37258 4 16C4 22.6274 9.37258 28 16 28Z" fill="#ECECEC"/>
                                                                <path d="M9.16154 16.9231C9.05385 16.8154 9 16.6538 9 16.5462C9 16.4385 9.05385 16.2769 9.16154 16.1692L9.91538 15.4154C10.1308 15.2 10.4538 15.2 10.6692 15.4154L10.7231 15.4692L13.6846 18.6462C13.7923 18.7538 13.9538 18.7538 14.0615 18.6462L21.2769 11.1615H21.3308C21.5462 10.9462 21.8692 10.9462 22.0846 11.1615L22.8385 11.9154C23.0538 12.1308 23.0538 12.4538 22.8385 12.6692L14.2231 21.6077C14.1154 21.7154 14.0077 21.7692 13.8462 21.7692C13.6846 21.7692 13.5769 21.7154 13.4692 21.6077L9.26923 17.0846L9.16154 16.9231Z" fill="white"/>
                                                            </svg>
                                                        </div>
                                                        <!-- <div class="float-right color-grey">
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </div> -->
                                                    </div>
                                                    <div class="padding-20 text-center">
                                                        <div class="bg-green mb-3">
                                                            <img src="images/watchicon.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <h5 class="text-capitalize font-weight-700 my-3">recent activity</h5>
                                                        <p class="color-grey">Showing individual people that recently signed up</p>
                                                    </div>
                                                </div>
                                                <div class="collapse show" id="notification1">
                                                    <div class="card card-body border-0 ">
                                                        <button type="button" class="btn linear-btn w-100" data-toggle="modal" data-target="#RecentActivityModal">
                                                            customize
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-4">
                                            <div class="box-shadow mb-0">
                                                <div data-toggle="collapse" href="#notification2" role="button" aria-expanded="false" aria-controls="notification2">
                                                    <div class="padding-20">
                                                        <div class="text-left d-inline">
                                                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16 31C24.2843 31 31 24.2843 31 16C31 7.71573 24.2843 1 16 1C7.71573 1 1 7.71573 1 16C1 24.2843 7.71573 31 16 31Z" fill="white" stroke="#EDEDED"/>
                                                                <path fill-rule="evenodd" id="active-fill" clip-rule="evenodd" d="M16 28C22.6274 28 28 22.6274 28 16C28 9.37258 22.6274 4 16 4C9.37258 4 4 9.37258 4 16C4 22.6274 9.37258 28 16 28Z" fill="#ECECEC"/>
                                                                <path d="M9.16154 16.9231C9.05385 16.8154 9 16.6538 9 16.5462C9 16.4385 9.05385 16.2769 9.16154 16.1692L9.91538 15.4154C10.1308 15.2 10.4538 15.2 10.6692 15.4154L10.7231 15.4692L13.6846 18.6462C13.7923 18.7538 13.9538 18.7538 14.0615 18.6462L21.2769 11.1615H21.3308C21.5462 10.9462 21.8692 10.9462 22.0846 11.1615L22.8385 11.9154C23.0538 12.1308 23.0538 12.4538 22.8385 12.6692L14.2231 21.6077C14.1154 21.7154 14.0077 21.7692 13.8462 21.7692C13.6846 21.7692 13.5769 21.7154 13.4692 21.6077L9.26923 17.0846L9.16154 16.9231Z" fill="white"/>
                                                            </svg>
                                                        </div>
                                                        <!-- <div class="float-right color-grey">
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </div> -->
                                                    </div>
                                                    <div class="padding-20 text-center">
                                                        <div class="mb-4">
                                                            <img src="images/livevisitor.png" class="img-fluid" alt="">
                                                        </div>
                                                        <h5 class="text-capitalize font-weight-700 my-3">Live Visitor Count</h5>
                                                        <p class="color-grey">Shows how many people are currently on your page</p>
                                                    </div>
                                                </div>
                                                <div class="collapse" id="notification2">
                                                    <div class="card card-body border-0 ">
                                                        <button type="button" class="btn linear-btn w-100" data-toggle="modal" data-target="#LiveVisitorModal">
                                                            customize
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-4">
                                            <div class="box-shadow mb-0">
                                                <div data-toggle="collapse" href="#notification3" role="button" aria-expanded="false" aria-controls="notification3">
                                                    <div class="padding-20">
                                                        <div class="text-left d-inline">
                                                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M16 31C24.2843 31 31 24.2843 31 16C31 7.71573 24.2843 1 16 1C7.71573 1 1 7.71573 1 16C1 24.2843 7.71573 31 16 31Z" fill="white" stroke="#EDEDED"/>
                                                                <path fill-rule="evenodd" id="active-fill" clip-rule="evenodd" d="M16 28C22.6274 28 28 22.6274 28 16C28 9.37258 22.6274 4 16 4C9.37258 4 4 9.37258 4 16C4 22.6274 9.37258 28 16 28Z" fill="#ECECEC"/>
                                                                <path d="M9.16154 16.9231C9.05385 16.8154 9 16.6538 9 16.5462C9 16.4385 9.05385 16.2769 9.16154 16.1692L9.91538 15.4154C10.1308 15.2 10.4538 15.2 10.6692 15.4154L10.7231 15.4692L13.6846 18.6462C13.7923 18.7538 13.9538 18.7538 14.0615 18.6462L21.2769 11.1615H21.3308C21.5462 10.9462 21.8692 10.9462 22.0846 11.1615L22.8385 11.9154C23.0538 12.1308 23.0538 12.4538 22.8385 12.6692L14.2231 21.6077C14.1154 21.7154 14.0077 21.7692 13.8462 21.7692C13.6846 21.7692 13.5769 21.7154 13.4692 21.6077L9.26923 17.0846L9.16154 16.9231Z" fill="white"/>
                                                            </svg>
                                                        </div>
                                                        <!-- <div class="float-right color-grey">
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </div> -->
                                                    </div>
                                                    <div class="padding-20 text-center">
                                                        <div class="bg-blue mb-3">
                                                            <img src="images/custom.svg" class="img-fluid" alt="">
                                                        </div>
                                                        <h5 class="text-capitalize font-weight-700 my-3">Custom</h5>
                                                        <p class="color-grey">You can Broadcast your ideas or messages here</p>
                                                    </div>
                                                </div>
                                                <div class="collapse" id="notification3">
                                                    <div class="card card-body border-0 ">
                                                        <button type="button" class="btn linear-btn w-100" data-toggle="modal" data-target="#CustomModal">
                                                            customize
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="pills-code" role="tabpanel" aria-labelledby="pills-code-tab">
                                <div class="pixel-code">
                                    <h3 class="text-center font-weight-700">Add this snippet to your site</h3>
                                    <p class="text-center color-grey">Copy the entire pixel base code and paste it.</p>
                                    <form action="" class="">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" placeholder="<!--MM CLICKPROOF PIXEL--><script src='https://marketermagic.com/api/clickproof/9d752fbb-69e-41246'></script><!--END MM CLICKPROOF PIXEL-->"></textarea>
                                        <!-- <button class="btn copy-code" type="button"><i class="fas fa-copy"></i></button> -->
                                        <button class="btn linear-btn linear-btn-shadow col-md-12 text-capitalize" type="button">test my pixel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade notification-modal" id="RecentActivityModal" tabindex="-1" role="dialog" aria-labelledby="RecentActivityModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-right modal-dialog-centered" role="document" style="position:unset">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="RecentActivityModalTitle">Recent Activity Notifications</h5>
                    <p class="d-block">Display individuals that recently converted on your site.</p>
                </div>
                <form action=""></form>
                <div class="modal-body padding-0">
                    <div class="padding-20 pb-5">
                        <div class="popup-modal">
                            <div class="circle-img">
                                <img src="images/map.png" class="img-fluid" alt="">
                            </div>
                            <p class="num">Dave from Austin, TX</p>
                            <div style="display: -webkit-box;">
                                <input type="text" placeholder="Edit Me" class="editme-input">
                                <p class="editme-text">Recently joined Club</p>
                                <button id="editme-button" type="button"><i class="fas fa-pen"></i></button>
                            </div>
                            <p class="verified">
                                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9914 6.10833L13.8093 7.39848C13.9208 7.57456 13.8922 7.80448 13.742 7.94879L12.6398 9.00579L12.7636 10.5275C12.7803 10.7359 12.6489 10.9265 12.4491 10.9841L10.9818 11.4068L10.3847 12.8127C10.3034 13.0044 10.0971 13.1137 9.89414 13.0706L8.39722 12.7637L7.21583 13.7313C7.13507 13.7965 7.03707 13.8304 6.93853 13.8304C6.83999 13.8304 6.74145 13.7965 6.66122 13.7313L5.47983 12.7637L3.98291 13.0706C3.77937 13.1148 3.57422 13.0039 3.49237 12.8127L2.89522 11.4068L1.42791 10.9841C1.22814 10.9259 1.09676 10.7359 1.11345 10.5275L1.2373 9.00579L0.135066 7.94879C-0.0151652 7.80448 -0.0437037 7.57456 0.0677579 7.39848L0.885143 6.10779L0.399989 4.65987C0.334296 4.46225 0.416681 4.24525 0.597066 4.14187L1.9206 3.37887L2.16399 1.87118C2.19737 1.66548 2.3713 1.51202 2.5786 1.50395L4.10514 1.44418L5.02107 0.221869C5.14653 0.0554841 5.3716 2.25194e-05 5.55899 0.0888687L6.93853 0.745253L8.31753 0.0894071C8.50437 -0.000515942 8.73053 0.0549456 8.85545 0.221869L9.77137 1.44418L11.2979 1.50395C11.5058 1.51256 11.6791 1.66602 11.7131 1.87171L11.9565 3.37941L13.28 4.14241C13.4604 4.24579 13.5422 4.46279 13.4765 4.66041L12.9914 6.10833ZM7.03868 9.88348L10.1031 5.29256C10.2242 5.1111 10.1779 4.86179 9.9986 4.73795L9.3476 4.29048C9.16883 4.16718 8.92276 4.21348 8.8016 4.39602L6.38714 8.01341L4.92307 6.52618C4.77068 6.37218 4.52137 6.37218 4.36791 6.52618L3.81276 7.09156C3.6593 7.24771 3.6593 7.50079 3.81276 7.65695L6.06245 9.94271C6.18899 10.0698 6.38768 10.1694 6.56537 10.1694C6.7436 10.1694 6.92291 10.0547 7.03868 9.88348Z" fill="#1B85FF"/>
                                </svg> <span>Verified by MarketerMagic</span>
                            </p>
                            <div class="close">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.597 17.954l-4.591-4.55-4.555 4.596-1.405-1.405 4.547-4.592-4.593-4.552 1.405-1.405 4.588 4.543 4.545-4.589 1.416 1.403-4.546 4.587 4.592 4.548-1.403 1.416z"></path></svg>
                            </div>
                        </div>
                    </div>
                    <div class="notification-msg padding-20 border-top">
                        <label for="">message <span class="color-red">*</span></label>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control" type="text" placeholder="Recently joined the Conversion Club">
                            <button class="btn preview-btn" id="preview" type="button"><i class="fas fa-eye"></i> Preview</button>
                        </form>
                    </div>
                    <div class="advance-settings padding-20 pt-0">
                        <h6 class="font-weight-700 text-capitalize">advanced settings</h6>
                        <a class="btn collapselink" data-toggle="collapse" href="#collapseimg" role="button" aria-expanded="false" aria-controls="collapseimg">
                           image options 
                           <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                <i class="fas fa-question-circle"></i>
                              </button>
                      
                         <i class="fas fa-angle-down"></i>
                        </a>
                        <div class="collapse" id="collapseimg">
                            <div class="card card-body border-0 rounded-0 padding-0 mt-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="displayimg">Display a custom image
                                            <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                <i class="fas fa-question-circle"></i>
                                            </button>
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="file-label" for="uploadfile">
                                                    choose file
                                                </label>
                                            <input type="file" class="form-control-file" id="uploadfile">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="btn collapselink" data-toggle="collapse" href="#displayrules" role="button" aria-expanded="false" aria-controls="displayrules">
                            display rules  <i class="fas fa-angle-down"></i>
                        </a>
                        <div class="collapse" id="displayrules">
                            <div class="card card-body border-0 rounded-0 padding-0 displayrulescollapse mt-3">
                                <p>Display the last <input type="text" placeholder="20"> Conversions </p>
                                <p>Only show conversions from the last <input type="text" placeholder="7"> days </p>
                                <p>Only display if there are at least <input type="text" placeholder="7"> Conversions </p>
                                <p>Always show most recent conversion on page load? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Prevent visitors from seeing their own notification? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" >
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Hide anonymous conversions? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Show all conversions as anonymous? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Do not loop notifications? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-start padding-20">
                    <button type="button" data-dismiss="modal" class="btn linear-btn">done</button>
                </div>
                </form>
            </div>

        </div>
        <div class="notification-preview RecentActivityPopup">
            <div class="popup-modal">
                <div class="circle-img">
                    <img src="images/map.png" class="img-fluid" alt="">
                </div>
                <p class="num">Dave from Austin, TX</p>
                <p>Recently joined the Conversion Club</p>
                <p class="verified">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9914 6.10833L13.8093 7.39848C13.9208 7.57456 13.8922 7.80448 13.742 7.94879L12.6398 9.00579L12.7636 10.5275C12.7803 10.7359 12.6489 10.9265 12.4491 10.9841L10.9818 11.4068L10.3847 12.8127C10.3034 13.0044 10.0971 13.1137 9.89414 13.0706L8.39722 12.7637L7.21583 13.7313C7.13507 13.7965 7.03707 13.8304 6.93853 13.8304C6.83999 13.8304 6.74145 13.7965 6.66122 13.7313L5.47983 12.7637L3.98291 13.0706C3.77937 13.1148 3.57422 13.0039 3.49237 12.8127L2.89522 11.4068L1.42791 10.9841C1.22814 10.9259 1.09676 10.7359 1.11345 10.5275L1.2373 9.00579L0.135066 7.94879C-0.0151652 7.80448 -0.0437037 7.57456 0.0677579 7.39848L0.885143 6.10779L0.399989 4.65987C0.334296 4.46225 0.416681 4.24525 0.597066 4.14187L1.9206 3.37887L2.16399 1.87118C2.19737 1.66548 2.3713 1.51202 2.5786 1.50395L4.10514 1.44418L5.02107 0.221869C5.14653 0.0554841 5.3716 2.25194e-05 5.55899 0.0888687L6.93853 0.745253L8.31753 0.0894071C8.50437 -0.000515942 8.73053 0.0549456 8.85545 0.221869L9.77137 1.44418L11.2979 1.50395C11.5058 1.51256 11.6791 1.66602 11.7131 1.87171L11.9565 3.37941L13.28 4.14241C13.4604 4.24579 13.5422 4.46279 13.4765 4.66041L12.9914 6.10833ZM7.03868 9.88348L10.1031 5.29256C10.2242 5.1111 10.1779 4.86179 9.9986 4.73795L9.3476 4.29048C9.16883 4.16718 8.92276 4.21348 8.8016 4.39602L6.38714 8.01341L4.92307 6.52618C4.77068 6.37218 4.52137 6.37218 4.36791 6.52618L3.81276 7.09156C3.6593 7.24771 3.6593 7.50079 3.81276 7.65695L6.06245 9.94271C6.18899 10.0698 6.38768 10.1694 6.56537 10.1694C6.7436 10.1694 6.92291 10.0547 7.03868 9.88348Z" fill="#1B85FF"/>
                    </svg> <span>Verified by MarketerMagic</span>
                </p>
                <div class="close" id="close">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.597 17.954l-4.591-4.55-4.555 4.596-1.405-1.405 4.547-4.592-4.593-4.552 1.405-1.405 4.588 4.543 4.545-4.589 1.416 1.403-4.546 4.587 4.592 4.548-1.403 1.416z"></path></svg>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade notification-modal" id="LiveVisitorModal" tabindex="-1" role="dialog" aria-labelledby="LiveVisitorModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-right modal-dialog-centered" role="document" style="position:unset">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="LiveVisitorModalTitle">Live Visitors Tracking</h5>
                    <p class="d-block">Display individuals that recently converted on your site.</p>
                </div>
                <form action=""></form>
                <div class="modal-body padding-0">
                    <div class="padding-20 pb-5">
                        <div class="popup-modal">
                            <div class="circle"></div>
                            <p class="num">84 people</p>
                            <div style="display: -webkit-box;">
                                <p class="editme-text">are viewing this page</p>
                            </div>
                            <p class="verified">
                                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9914 6.10833L13.8093 7.39848C13.9208 7.57456 13.8922 7.80448 13.742 7.94879L12.6398 9.00579L12.7636 10.5275C12.7803 10.7359 12.6489 10.9265 12.4491 10.9841L10.9818 11.4068L10.3847 12.8127C10.3034 13.0044 10.0971 13.1137 9.89414 13.0706L8.39722 12.7637L7.21583 13.7313C7.13507 13.7965 7.03707 13.8304 6.93853 13.8304C6.83999 13.8304 6.74145 13.7965 6.66122 13.7313L5.47983 12.7637L3.98291 13.0706C3.77937 13.1148 3.57422 13.0039 3.49237 12.8127L2.89522 11.4068L1.42791 10.9841C1.22814 10.9259 1.09676 10.7359 1.11345 10.5275L1.2373 9.00579L0.135066 7.94879C-0.0151652 7.80448 -0.0437037 7.57456 0.0677579 7.39848L0.885143 6.10779L0.399989 4.65987C0.334296 4.46225 0.416681 4.24525 0.597066 4.14187L1.9206 3.37887L2.16399 1.87118C2.19737 1.66548 2.3713 1.51202 2.5786 1.50395L4.10514 1.44418L5.02107 0.221869C5.14653 0.0554841 5.3716 2.25194e-05 5.55899 0.0888687L6.93853 0.745253L8.31753 0.0894071C8.50437 -0.000515942 8.73053 0.0549456 8.85545 0.221869L9.77137 1.44418L11.2979 1.50395C11.5058 1.51256 11.6791 1.66602 11.7131 1.87171L11.9565 3.37941L13.28 4.14241C13.4604 4.24579 13.5422 4.46279 13.4765 4.66041L12.9914 6.10833ZM7.03868 9.88348L10.1031 5.29256C10.2242 5.1111 10.1779 4.86179 9.9986 4.73795L9.3476 4.29048C9.16883 4.16718 8.92276 4.21348 8.8016 4.39602L6.38714 8.01341L4.92307 6.52618C4.77068 6.37218 4.52137 6.37218 4.36791 6.52618L3.81276 7.09156C3.6593 7.24771 3.6593 7.50079 3.81276 7.65695L6.06245 9.94271C6.18899 10.0698 6.38768 10.1694 6.56537 10.1694C6.7436 10.1694 6.92291 10.0547 7.03868 9.88348Z" fill="#1B85FF"/>
                                </svg> <span>Verified by MarketerMagic</span>
                            </p>
                            <div class="close">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.597 17.954l-4.591-4.55-4.555 4.596-1.405-1.405 4.547-4.592-4.593-4.552 1.405-1.405 4.588 4.543 4.545-4.589 1.416 1.403-4.546 4.587 4.592 4.548-1.403 1.416z"></path></svg>
                            </div>
                        </div>
                    </div>
                    <div class="advance-settings padding-20 pt-0">
                        <a href="#" class="btn collapselink">
                            display rules 
                        </a>
                        <div class="" id="">
                            <div class="card card-body border-0 rounded-0 padding-0 displayrulescollapse mt-3">
                                <p>Display the number of live visitors on the entire domain?<button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                    <i class="fas fa-question-circle"></i>
                                    </button>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Only display if there are at least <input type="text" placeholder="7"> active visitors </p>
                                <p>Refer to my visitors as<input type="text" placeholder="people" style="    width: 70px;"> </p>
                            </div>
                        </div>
                        <button class="btn preview-btn" id="preview1" type="button"><i class="fas fa-eye"></i> Preview</button>
                    </div>
                </div>
                <div class="modal-footer justify-content-start padding-20">
                    <button type="button" data-dismiss="modal" class="btn linear-btn">done</button>
                </div>
                </form>
            </div>

        </div>
        <div class="notification-preview LiveVisitorPopup">
            <div class="popup-modal">
                <div class="circle"></div>
                <p class="num">84 people</p>
                <p>are viewing this page</p>
                <p class="verified">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9914 6.10833L13.8093 7.39848C13.9208 7.57456 13.8922 7.80448 13.742 7.94879L12.6398 9.00579L12.7636 10.5275C12.7803 10.7359 12.6489 10.9265 12.4491 10.9841L10.9818 11.4068L10.3847 12.8127C10.3034 13.0044 10.0971 13.1137 9.89414 13.0706L8.39722 12.7637L7.21583 13.7313C7.13507 13.7965 7.03707 13.8304 6.93853 13.8304C6.83999 13.8304 6.74145 13.7965 6.66122 13.7313L5.47983 12.7637L3.98291 13.0706C3.77937 13.1148 3.57422 13.0039 3.49237 12.8127L2.89522 11.4068L1.42791 10.9841C1.22814 10.9259 1.09676 10.7359 1.11345 10.5275L1.2373 9.00579L0.135066 7.94879C-0.0151652 7.80448 -0.0437037 7.57456 0.0677579 7.39848L0.885143 6.10779L0.399989 4.65987C0.334296 4.46225 0.416681 4.24525 0.597066 4.14187L1.9206 3.37887L2.16399 1.87118C2.19737 1.66548 2.3713 1.51202 2.5786 1.50395L4.10514 1.44418L5.02107 0.221869C5.14653 0.0554841 5.3716 2.25194e-05 5.55899 0.0888687L6.93853 0.745253L8.31753 0.0894071C8.50437 -0.000515942 8.73053 0.0549456 8.85545 0.221869L9.77137 1.44418L11.2979 1.50395C11.5058 1.51256 11.6791 1.66602 11.7131 1.87171L11.9565 3.37941L13.28 4.14241C13.4604 4.24579 13.5422 4.46279 13.4765 4.66041L12.9914 6.10833ZM7.03868 9.88348L10.1031 5.29256C10.2242 5.1111 10.1779 4.86179 9.9986 4.73795L9.3476 4.29048C9.16883 4.16718 8.92276 4.21348 8.8016 4.39602L6.38714 8.01341L4.92307 6.52618C4.77068 6.37218 4.52137 6.37218 4.36791 6.52618L3.81276 7.09156C3.6593 7.24771 3.6593 7.50079 3.81276 7.65695L6.06245 9.94271C6.18899 10.0698 6.38768 10.1694 6.56537 10.1694C6.7436 10.1694 6.92291 10.0547 7.03868 9.88348Z" fill="#1B85FF"/>
                    </svg> <span>Verified by MarketerMagic</span>
                </p>
                <div class="close" id="close1">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.597 17.954l-4.591-4.55-4.555 4.596-1.405-1.405 4.547-4.592-4.593-4.552 1.405-1.405 4.588 4.543 4.545-4.589 1.416 1.403-4.546 4.587 4.592 4.548-1.403 1.416z"></path></svg>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade notification-modal" id="CustomModal" tabindex="-1" role="dialog" aria-labelledby="CustomModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-right modal-dialog-centered" role="document" style="position:unset">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="CustomModalTitle">Custom</h5>
                    <p class="d-block">Make any optional customizations to your campaign below.</p>
                </div>
                <form action=""></form>
                <div class="modal-body padding-0">
                    <div class="padding-20 pb-5">
                        <div class="popup-modal">
                            <div class="circle"></div>
                            <p class="num">Dave from Austin, TX</p>
                            <div style="display: -webkit-box;">
                                <input type="text" placeholder="Edit Me" class="editme-input">
                                <p class="editme-text">Recently joined Club</p>
                                <button id="editme-button" type="button"><i class="fas fa-pen"></i></button>
                            </div>
                            <p class="verified">
                                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9914 6.10833L13.8093 7.39848C13.9208 7.57456 13.8922 7.80448 13.742 7.94879L12.6398 9.00579L12.7636 10.5275C12.7803 10.7359 12.6489 10.9265 12.4491 10.9841L10.9818 11.4068L10.3847 12.8127C10.3034 13.0044 10.0971 13.1137 9.89414 13.0706L8.39722 12.7637L7.21583 13.7313C7.13507 13.7965 7.03707 13.8304 6.93853 13.8304C6.83999 13.8304 6.74145 13.7965 6.66122 13.7313L5.47983 12.7637L3.98291 13.0706C3.77937 13.1148 3.57422 13.0039 3.49237 12.8127L2.89522 11.4068L1.42791 10.9841C1.22814 10.9259 1.09676 10.7359 1.11345 10.5275L1.2373 9.00579L0.135066 7.94879C-0.0151652 7.80448 -0.0437037 7.57456 0.0677579 7.39848L0.885143 6.10779L0.399989 4.65987C0.334296 4.46225 0.416681 4.24525 0.597066 4.14187L1.9206 3.37887L2.16399 1.87118C2.19737 1.66548 2.3713 1.51202 2.5786 1.50395L4.10514 1.44418L5.02107 0.221869C5.14653 0.0554841 5.3716 2.25194e-05 5.55899 0.0888687L6.93853 0.745253L8.31753 0.0894071C8.50437 -0.000515942 8.73053 0.0549456 8.85545 0.221869L9.77137 1.44418L11.2979 1.50395C11.5058 1.51256 11.6791 1.66602 11.7131 1.87171L11.9565 3.37941L13.28 4.14241C13.4604 4.24579 13.5422 4.46279 13.4765 4.66041L12.9914 6.10833ZM7.03868 9.88348L10.1031 5.29256C10.2242 5.1111 10.1779 4.86179 9.9986 4.73795L9.3476 4.29048C9.16883 4.16718 8.92276 4.21348 8.8016 4.39602L6.38714 8.01341L4.92307 6.52618C4.77068 6.37218 4.52137 6.37218 4.36791 6.52618L3.81276 7.09156C3.6593 7.24771 3.6593 7.50079 3.81276 7.65695L6.06245 9.94271C6.18899 10.0698 6.38768 10.1694 6.56537 10.1694C6.7436 10.1694 6.92291 10.0547 7.03868 9.88348Z" fill="#1B85FF"/>
                                </svg> <span>Verified by MarketerMagic</span>
                            </p>
                            <div class="close">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.597 17.954l-4.591-4.55-4.555 4.596-1.405-1.405 4.547-4.592-4.593-4.552 1.405-1.405 4.588 4.543 4.545-4.589 1.416 1.403-4.546 4.587 4.592 4.548-1.403 1.416z"></path></svg>
                            </div>
                        </div>
                    </div>
                    <div class="notification-msg padding-20 border-top">
                        <label for="">message <span class="color-red">*</span></label>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control" type="text" placeholder="Recently joined the Conversion Club">
                            <button class="btn preview-btn" id="preview2" type="button"><i class="fas fa-eye"></i> Preview</button>
                        </form>
                    </div>
                    <div class="advance-settings padding-20 pt-0">
                        <h6 class="font-weight-700 text-capitalize">advanced settings</h6>
                        <a class="btn collapselink" data-toggle="collapse" href="#collapseimg" role="button" aria-expanded="false" aria-controls="collapseimg">
                           image options 
                           <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                <i class="fas fa-question-circle"></i>
                            </button>
                         <i class="fas fa-angle-down"></i>
                        </a>
                        <div class="collapse" id="collapseimg">
                            <div class="card card-body border-0 rounded-0 padding-0 mt-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="displayimg">Display a custom image
                                            <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                <i class="fas fa-question-circle"></i>
                                            </button>
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="file-label" for="uploadfile">
                                                    choose file
                                                </label>
                                            <input type="file" class="form-control-file" id="uploadfile">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="btn collapselink" data-toggle="collapse" href="#displayrules" role="button" aria-expanded="false" aria-controls="displayrules">
                            display rules  <i class="fas fa-angle-down"></i>
                        </a>
                        <div class="collapse" id="displayrules">
                            <div class="card card-body border-0 rounded-0 padding-0 displayrulescollapse mt-3">
                                <p>Display the last <input type="text" placeholder="20"> Conversions </p>
                                <p>Only show conversions from the last <input type="text" placeholder="7"> days </p>
                                <p>Only display if there are at least <input type="text" placeholder="7"> Conversions </p>
                                <p>Always show most recent conversion on page load? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Prevent visitors from seeing their own notification? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" >
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Hide anonymous conversions? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Show all conversions as anonymous? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                                <p>Do not loop notifications? <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                        <i class="fas fa-question-circle"></i>
                                      </button>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-start padding-20">
                    <button type="button" data-dismiss="modal" class="btn linear-btn">done</button>
                </div>
                </form>
            </div>

        </div>
        <div class="notification-preview CustomPopup">
            <div class="popup-modal">
                <div class="circle"></div>
                <p class="num">Dave from Austin, TX</p>
                <p>Recently joined the Conversion Club</p>
                <p class="verified">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9914 6.10833L13.8093 7.39848C13.9208 7.57456 13.8922 7.80448 13.742 7.94879L12.6398 9.00579L12.7636 10.5275C12.7803 10.7359 12.6489 10.9265 12.4491 10.9841L10.9818 11.4068L10.3847 12.8127C10.3034 13.0044 10.0971 13.1137 9.89414 13.0706L8.39722 12.7637L7.21583 13.7313C7.13507 13.7965 7.03707 13.8304 6.93853 13.8304C6.83999 13.8304 6.74145 13.7965 6.66122 13.7313L5.47983 12.7637L3.98291 13.0706C3.77937 13.1148 3.57422 13.0039 3.49237 12.8127L2.89522 11.4068L1.42791 10.9841C1.22814 10.9259 1.09676 10.7359 1.11345 10.5275L1.2373 9.00579L0.135066 7.94879C-0.0151652 7.80448 -0.0437037 7.57456 0.0677579 7.39848L0.885143 6.10779L0.399989 4.65987C0.334296 4.46225 0.416681 4.24525 0.597066 4.14187L1.9206 3.37887L2.16399 1.87118C2.19737 1.66548 2.3713 1.51202 2.5786 1.50395L4.10514 1.44418L5.02107 0.221869C5.14653 0.0554841 5.3716 2.25194e-05 5.55899 0.0888687L6.93853 0.745253L8.31753 0.0894071C8.50437 -0.000515942 8.73053 0.0549456 8.85545 0.221869L9.77137 1.44418L11.2979 1.50395C11.5058 1.51256 11.6791 1.66602 11.7131 1.87171L11.9565 3.37941L13.28 4.14241C13.4604 4.24579 13.5422 4.46279 13.4765 4.66041L12.9914 6.10833ZM7.03868 9.88348L10.1031 5.29256C10.2242 5.1111 10.1779 4.86179 9.9986 4.73795L9.3476 4.29048C9.16883 4.16718 8.92276 4.21348 8.8016 4.39602L6.38714 8.01341L4.92307 6.52618C4.77068 6.37218 4.52137 6.37218 4.36791 6.52618L3.81276 7.09156C3.6593 7.24771 3.6593 7.50079 3.81276 7.65695L6.06245 9.94271C6.18899 10.0698 6.38768 10.1694 6.56537 10.1694C6.7436 10.1694 6.92291 10.0547 7.03868 9.88348Z" fill="#1B85FF"/>
                    </svg> <span>Verified by MarketerMagic</span>
                </p>
                <div class="close" id="close2">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.597 17.954l-4.591-4.55-4.555 4.596-1.405-1.405 4.547-4.592-4.593-4.552 1.405-1.405 4.588 4.543 4.545-4.589 1.416 1.403-4.546 4.587 4.592 4.548-1.403 1.416z"></path></svg>
                </div>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/bootstrap-tagsinput.js"></script>
</body>

</html>