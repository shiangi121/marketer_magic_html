<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <title>Marketer Magic</title>
    <style>
        .main-header form,
        .main-header form input::placeholder {
            color: #ffffff;
        }
        
        .main-header .dropdown-toggle {
            color: #ffffff;
        }
        
        #notificationdropdown svg path {
            fill: #fff;
        }
        
        .referral-menu {
            color: #fff !important;
        }
        
        #userdropdown svg path {
            stroke: #fff;
        }
    </style>
</head>

<body >
    <div class="wrapper">

        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>

        <!-- Page Content  -->
        <div id="content" class="active">
            <div id="dashboard" class="pb-50">
                <div class="dashboard-enterscreen text-center">
                    <div class="">
                        <div class="col-md-12">
                            <h5>Hi There! <img src="images/new-images/smiley.svg" style="position: relative;top: -5px;" class="img-fluid" alt=""> </h5>
                            <p>Welcome to Marketer Magic! The Top Social Proof Conversion Platform on the Planet - Let’s Get Started!</p>
                        </div>
                    </div>
                </div>

                <div class="dash-quickstart mt-5">
                    <div class="row">
                        <div class="start_arrow">
                            <img src="images/Start-Here-white.png" alt="">
                        </div>
                        <div class="black_overlay"></div>

                        <div class="col-lg-3 col-md-12 responsive-ipad start_column">
                            <div class="white_box box-shadow text-center custombox-height">
                                <img src="images/new-images/mu-logo.svg" class="img-fluid mt-4" alt="">
                                <p class="mu-text">Marketers Who Joined The University Makes 100% More Profit!</p>
                                <a href="#" target="_blank" class="mu-btn btn text-capitalize">start here</a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 responsive-ipadimg responsive-ipad text-center" style="margin-bottom: 30px;">
                            <img src="images/new-images/mm_welcome_image.gif" class="img-fluid custombox-height" alt="">
                        </div>
                        <div class="col-lg-3 col-md-12 responsive-ipad">
                            <div class="box-shadow text-center custombox-height">
                                <div class="trophy-img">
                                    <img src="images/new-images/mu-trophy.svg" class="img-fluid" alt="">
                                </div>
                                <h5 class="font-weight-700" style="margin-top: -20px; font-size: 16px;">Hall of Fame Marketers</h5>
                                <ul class="nav nav-pills mb-0 halloffame-tab" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-weekly-tab" data-toggle="pill" href="#pills-weekly" role="tab" aria-controls="pills-weekly" aria-selected="true">weekly</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-monthly-tab" data-toggle="pill" href="#pills-monthly" role="tab" aria-controls="pills-monthly" aria-selected="false">monthly</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-overall-tab" data-toggle="pill" href="#pills-overall" role="tab" aria-controls="pills-overall" aria-selected="false">overall</a>
                                    </li>
                                </ul>
                                <div class="tab-content halloffame-tabcontent padding-20" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-weekly" role="tabpanel" aria-labelledby="pills-weekly-tab">
                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/yellow-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Adrian Smith</td>
                                                        <td class="text-right">$5,503</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/grey-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Jennifer Wyne</td>
                                                        <td class="text-right">$4,746</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/orange-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Daniel Powter</td>
                                                        <td class="text-right">$3,243</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>4</b>
                                                        </td>
                                                        <td class="text-left text-capitalize">Jacob Miller</td>
                                                        <td class="text-right">$970</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>5</b>
                                                        </td>
                                                        <td class="text-left text-capitalize">Alex Chang</td>
                                                        <td class="text-right">$575</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-monthly" role="tabpanel" aria-labelledby="pills-monthly-tab">
                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/yellow-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Adrian Smith</td>
                                                        <td class="text-right">$5,503</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/grey-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Jennifer Wyne</td>
                                                        <td class="text-right">$4,746</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-overall" role="tabpanel" aria-labelledby="pills-overall-tab">
                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/yellow-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Adrian Smith</td>
                                                        <td class="text-right">$5,503</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/grey-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Jennifer Wyne</td>
                                                        <td class="text-right">$4,746</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/new-images/orange-bedge.svg" class="img-fluid" alt="">
                                                        </td>
                                                        <td class="text-left text-capitalize">Daniel Powter</td>
                                                        <td class="text-right">$3,243</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>4</b>
                                                        </td>
                                                        <td class="text-left text-capitalize">Jacob Miller</td>
                                                        <td class="text-right">$970</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>5</b>
                                                        </td>
                                                        <td class="text-left text-capitalize">Alex Chang</td>
                                                        <td class="text-right">$575</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://affiliate.marketermagic.com/" target="_blank" class="mu-btn btn text-uppercase">Get $300</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <a href="clickproof" class="color-black">
                                <div class="box-shadow responsive-height-box text-center">
                                    <img src="images/new-images/clickproof_icon.svg" class="img-fluid my-4" alt="">
                                    <div class="content">
                                        <h5 class="text-capitalize"><b>ClickProof</b></h5>
                                        <p> <span class="main-color">10,000+ </span> Users viewed your ClickProof in last 7 days!</p>
                                    </div>
                                    <div class="border-top">
                                        <button class="btn launch-btn" onclick="window.location.href='clickproof'" type="button">launch now
                                            <img src="images/new-images/right-white.svg" class="img-fluid ml-3" alt=""></button>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <a href="{{ route('user.picSnippets.index" class="color-black">
                                <div class="box-shadow responsive-height-box text-center">
                                    <img src="images/new-images/livepic_icon.svg" class="img-fluid my-4" alt="">
                                    <div class="content">
                                        <h5 class="text-capitalize"><b>livePic</b></h5>
                                        <p> <span class="main-color">10,000+</span> Users viewed your LivePic in last 7 days!</p>
                                    </div>
                                    <div class="border-top">
                                        <button class="btn launch-btn" onclick="window.location.href='{{ route('user.picSnippets.index'" type="button">launch now
                                            <img src="images/new-images/right-white.svg" class="img-fluid ml-3" alt=""> </button>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <a href="ev" class="color-black">
                                <div class="box-shadow responsive-height-box text-center">
                                    <img src="images/new-images/emailverify_icon.svg" class="img-fluid my-4" alt="">
                                    <div class="content">
                                        <h5 class="text-capitalize"><b>EmailVerifier </b></h5>
                                        <p> <span class="main-color">10,000+</span> Mails have been verified by you in last 7 days!</p>
                                    </div>
                                    <div class="border-top">
                                        <button class="btn launch-btn" onclick="window.location.href='ev'" type="button">launch now
                                            <img src="images/new-images/right-white.svg" class="img-fluid ml-3" alt=""> </button>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <a href="workhub" class="color-black">
                                <div class="box-shadow responsive-height-box text-center">
                                    <img src="images/new-images/workhub_icon.svg" class="img-fluid my-4" alt="">
                                    <div class="content">
                                        <h5 class="text-capitalize"><b>WorkHub </b></h5>
                                        <p> Your employees worked <span class="main-color">10,000+</span> hours in last 7 days!</p>
                                    </div>
                                    <div class="border-top">
                                        <button class="btn launch-btn" onclick="window.location.href='workhub'" type="button">launch now
                                            <img src="images/new-images/right-white.svg" class="img-fluid ml-3" alt=""></button>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <a href="manyleads/campaigns" class="color-black">
                                <div class="box-shadow responsive-height-box text-center">
                                    <img src="images/new-images/manyleads_icon.svg" class="img-fluid my-4" alt="">
                                    <div class="content">
                                        <h5 class="text-capitalize"><b>ManyLeads</b></h5>
                                        <p> <span class="main-color">10,000+</span> leads were Discovered by you in last 7 days!</p>
                                    </div>
                                    <div class="border-top">
                                        <button class="btn launch-btn" onclick="window.location.href='manyleads/campaigns'" type="button">launch now
                                            <img src="images/new-images/right-white.svg" class="img-fluid ml-3" alt=""> </button>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <a href="minime" class="color-black">
                                <div class="box-shadow responsive-height-box text-center">
                                    <img src="images/new-images/minime_icon.svg" class="img-fluid my-4" alt="">
                                    <div class="content">
                                        <h5 class="text-capitalize"><b>MiniMe </b></h5>
                                        <p> <span class="main-color">10,000+</span> Clicks have been recorded on your links in last 7 days!</p>
                                    </div>
                                    <div class="border-top">
                                        <button class="btn launch-btn" onclick="window.location.href='minime'" type="button">launch now
                                            <img src="images/new-images/right-white.svg" class="img-fluid ml-3" alt=""></button>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>


            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script type="text/javascript">
        function DropList() {
            var n = document.getElementById("sel").options.length;
            document.getElementById("sel").size = n;
        }
    </script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script>
        $(function() {
            var tl = new TimelineMax();

            tl
                .to(".black_overlay", 1, {
                    opacity: 1
                })
                .to(".start_column", 1, {
                    opacity: 1,
                    y: -10
                }, "=-.5")
                .to(".start_arrow", 1, {
                    opacity: 1,
                    x: -40
                }, "=-.5");

            $('.custombox-height').removeClass(".box-shadow");

        });
        $(".black_overlay").click(function() {
            $('.black_overlay, .start_arrow').hide();

            TweenMax.to(".start_column", 1, {
                y: 0
            });
        })
    </script>
</body>

</html>