<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/setting-billing.css">
    <link rel="stylesheet" href="css/planupgrade.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>
    <style>
        .main-header form,
        .main-header form input::placeholder {
            color: #ffffff;
        }
        
        .main-header .dropdown-toggle {
            color: #ffffff;
        }
        
        #notificationdropdown svg path {
            fill: #fff;
        }
        
        .referral-menu {
            color: #fff !important;
        }
        
        #userdropdown svg path {
            stroke: #fff;
        }
    </style>
</head>

<body>
    <div class="wrapper">

        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>

        <!-- Page Content  -->
        <div id="content" class="active">

            <div id="settings" class="pb-50">
                <div class="settings-banner"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="account-settings text-center">
                                <img src="images/userimg.png" class="img-fluid user-img" alt="">
                                <a href="#" class="change-pic main-color text-capitalize">chaneg picture</a>
                                <div class="mt-4">
                                    <h5 class="d-inline font-weight-600">Welcome,</h5>
                                    <input type="text" placeholder="Adrian Smith" class="editname-input">
                                    <h5 class="edit-name font-weight-600"> Adrian Smith</h5>
                                    <button id="editname-button" type="button"> <i class="fas fa-pen"></i></button>
                                </div>
                                <p class="color-grey my-3">Manage your MarketerMagic account, billing, and apps</p>
                                <div class="setting-options">
                                    <ul class="nav nav-pills justify-content-center mb-0" style="border-bottom: 1px solid #EFEFEF;" id="pills-tab" role="tablist">
                                        <li class="nav-item col-md-3">
                                            <a class="nav-link" id="pills-acoount-tab" data-toggle="pill" href="#pills-acoount" role="tab" aria-controls="pills-acoount" aria-selected="true">acoount</a>
                                        </li>
                                        <li class="nav-item col-md-3 wizard-border-left wizard-border-right">
                                            <a class="nav-link" id="pills-loginmethod-tab" data-toggle="pill" href="#pills-loginmethod" role="tab" aria-controls="pills-loginmethod" aria-selected="false">login method</a>
                                        </li>
                                        <li class="nav-item col-md-3 wizard-border-right">
                                            <a class="nav-link" id="pills-billing-tab" data-toggle="pill" href="#pills-billing" role="tab" aria-controls="pills-billing" aria-selected="false">billing <span class="billing-dot"></span></a>
                                        </li>
                                        <li class="nav-item col-md-3">
                                            <a class="nav-link active" id="pills-plan-tab" data-toggle="pill" href="#pills-plan" role="tab" aria-controls="pills-plan" aria-selected="false">plan </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade" id="pills-acoount" role="tabpanel" aria-labelledby="pills-acoount-tab">
                                            <form action="" class="account-form text-left padding-30">
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label for="validationDefault01">First name</label>
                                                        <input type="text" class="form-control" id="validationDefault01" placeholder="First name" value="Mark" required>
                                                    </div>
                                                    <div class="col-md-6 mb-3">
                                                        <label for="validationDefault02">Last name</label>
                                                        <input type="text" class="form-control" id="validationDefault02" placeholder="Last name" value="Otto" required>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-3">
                                                        <label for="compnyname">Compnay Name</label>
                                                        <input type="text" class="form-control" id="compnyname" value="AVDEVS" placeholder="Compnay Name" required>
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label for="phone">Phone</label>
                                                        <input type="tel" class="form-control" id="phone" value="xxx-xxxx-xxxx" placeholder="Phone" required>
                                                    </div>
                                                    <div class="col-md-6 mb-3">
                                                        <label for="inputEmail">Email</label>
                                                        <input type="email" class="form-control" id="inputEmail" value="elon.mush@gmail.com" placeholder="Email" disabled required>
                                                    </div>
                                                </div>
                                                <button class="btn linear-btn my-3" type="submit">Save Profile</button>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="pills-loginmethod" role="tabpanel" aria-labelledby="pills-loginmethod-tab">
                                            <form action="" class="setpswd-form text-left padding-30 px-5">
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-3">
                                                        <label for="currentpswd">Current Password</label>
                                                        <input type="password" class="form-control" id="currentpswd" value="" placeholder="" required>
                                                    </div>
                                                    <div class="col-md-12 mb-3">
                                                        <label for="newpswd">New Password</label>
                                                        <input type="password" class="form-control" id="newpswd" value="" placeholder="" required>
                                                    </div>
                                                    <div class="col-md-12 mb-3">
                                                        <label for="confirmpswd">Confirm Password</label>
                                                        <input type="password" class="form-control" id="confirmpswd" value="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <button class="btn linear-btn my-3" type="submit">Save Password</button>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="pills-billing" role="tabpanel" aria-labelledby="pills-billing-tab">
                                            <form action="" class="billing-form text-left padding-30">

                                                <div class="form-row">
                                                    <p>billing details</p>
                                                    <div class="col-md-12 mb-3">
                                                        <label for="inputEmail">Billing Email</label>
                                                        <input type="email" class="form-control" id="inputEmail" value="elon.mush@gmail.com" placeholder="Email" required>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <p>update my payment info</p>
                                                    <div class="col-md-12 mb-3">
                                                        <label for="carddetail">credit card details</label>
                                                        <input type="tel" class="form-control card-bgimg" id="carddetail" value="" placeholder="Card Number" required>
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <input type="tel" class="form-control" id="date" placeholder="MM/YY" required>
                                                    </div>
                                                    <div class="col-md-3 mb-3">
                                                        <input type="tel" class="form-control" id="cvv" placeholder="CVV" required>
                                                    </div>
                                                </div>
                                                <button class="btn linear-btn my-3" type="submit">Submit</button>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade show active" id="pills-plan" role="tabpanel" aria-labelledby="pills-plan-tab">
                                            <div class="plan-box text-left padding-30">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p class="current-plan mb-2">current plans</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="mb-2 text-capitalize paygrow-plan">ClickProof - $29/m</p>
                                                        <p class="mb-2 text-capitalize paygrow-plan">LivePic - $29/m</p>
                                                        <p class="mb-2 text-capitalize paygrow-plan">MiniMe - $29/m</p>
                                                    </div>
                                                    <div class="col-md-6 text-right mobile-left">
                                                        <button onclick="window.location.href='pricing-page.php'" class="upgrade-planbtn btn text-capitalize" type="button">upgrade plan</button>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <p class="next-billing-date mt-3"><span class="main-color">$1,164.00 Billed Annual</span> - Next billing date: November 12th, 2020.</p>
                                                        <p class="free-plan-text mt-3">If You'd Like to Upgrade Multiple Apps at Once Please Select
                                                            <b>'Upgrade Plan'</b> and Choose the Baller Option for Full Access to Our Suite! </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="monthlyannual-box">
                                                            <ul class="nav nav-pills monthly-annual-plantab justify-content-center" id="pills-tab" role="tablist">
                                                                <li class="nav-item" style="    border-right: none;
                                                                    border-bottom-right-radius: 0;
                                                                    border-top-right-radius: 0;">
                                                                    <a class="nav-link active" id="pills-monthly-tab" data-toggle="pill" href="#pills-monthly" role="tab" aria-controls="pills-monthly" aria-selected="true">monthly</a>
                                                                </li>
                                                                <li class="nav-item" style="border-left: none;
                                                                    border-bottom-left-radius: 0;
                                                                    border-top-left-radius: 0;">
                                                                    <a class="nav-link" id="pills-annual-tab" data-toggle="pill" href="#pills-annual" role="tab" aria-controls="pills-annual" aria-selected="false">annual</a>
                                                                </li>
                                                            </ul>
                                                            <div class="tab-content monthly-annual-plantabcontent" id="pills-tabContent">
                                                                <div class="tab-pane fade  show active" id="pills-monthly" role="tabpanel" aria-labelledby="pills-monthly-tab">
                                                                    <div class="table-responsive px-4">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="text-left"></th>
                                                                                    <th class="text-right">
                                                                                        <p class="main-color text-uppercase mb-0">MONTHLY USAGE</p>
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 main-color text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> ClickProof </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage">1000 / <span class="font-weight-900">1000 Visits</span></p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 main-color text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> LivePic </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1000 / <span class="font-weight-900">1000 Photos</span></p>

                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 color-grey text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> EmailVerifier </p>
                                                                                        <p class="module-price">
                                                                                            Only 1¢ per email
                                                                                        </p>
                                                                                    </td>

                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1000 / <span class="font-weight-900">1000 Emails</span></p>
                                                                                        <button type="button" class="btn module-upgrade">Clean a List</button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 color-grey text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> WorkHub </p>
                                                                                        <p class="module-price">
                                                                                            $4/User
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1 / <span class="font-weight-900">1 User</span></p>
                                                                                        <!-- <button type="button" class="btn module-upgrade">Add User</button> -->
                                                                                        <div class="module-upgrade workhub-user">

                                                                                            <button type="button" class="btn btn-number padding-0" data-type="minus" data-field="quant[1]">
                                                                                                       
                                                                                                            <i class="fas fa-minus"></i>
                                                                                                    </button>

                                                                                            <input type="text" name="quant[1]" class="input-number" value="Add User" min="1" max="100" placeholder="Add User">

                                                                                            <button type="button" class="btn btn-number padding-0" data-type="plus" data-field="quant[1]">
                                                                                                            <i class="fas fa-plus"></i>
                                                                                                 
                                                                                                        </button>

                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 color-grey text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> ManyLeads </p>
                                                                                        <p class="module-price">
                                                                                            Only $29/Month!
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1 / <span class="font-weight-900">1 Search</span></p>
                                                                                        <button type="button" class="btn module-upgrade">Upgrade Now!</button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 main-color text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> MiniMe </p>
                                                                                    </td>

                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1000 / <span class="font-weight-900">1000 Clicks</span></p>

                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane fade" id="pills-annual" role="tabpanel" aria-labelledby="pills-annual-tab">
                                                                    <div class="table-responsive px-4">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="text-left"></th>
                                                                                    <th class="text-right">
                                                                                        <p class="main-color text-uppercase mb-0">annual USAGE</p>
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 main-color text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> ClickProof </p>

                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage">1000 / <span class="font-weight-900">1000 Visits</span></p>

                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 main-color text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> LivePic </p>

                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1000 / <span class="font-weight-900">1000 Photos</span></p>

                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 color-grey text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> EmailVerifier </p>
                                                                                        <p class="module-price">
                                                                                            Only 1¢ per email
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1000 / <span class="font-weight-900">1000 Emails</span></p>
                                                                                        <button type="button" class="btn module-upgrade">Clean a List</button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 color-grey text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> WorkHub </p>
                                                                                        <p class="module-price">
                                                                                            $4/User
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1 / <span class="font-weight-900">1 User</span></p>
                                                                                        <!-- <button type="button" class="btn module-upgrade">Add User</button> -->
                                                                                        <div class="module-upgrade workhub-user">

                                                                                            <button type="button" class="btn btn-number padding-0" data-type="minus" data-field="quant[1]">
                                                                                               
                                                                                                    <i class="fas fa-minus"></i>
                                                                                            </button>

                                                                                            <input type="text" name="quant[1]" class="input-number" value="Add User" min="1" max="100" placeholder="Add User">

                                                                                            <button type="button" class="btn btn-number padding-0" data-type="plus" data-field="quant[1]">
                                                                                                    <i class="fas fa-plus"></i>
                                                                                            </button>

                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 color-grey text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> ManyLeads </p>
                                                                                        <p class="module-price">
                                                                                            Only $19/Month! <span class="main-color">(SAVE 40%!)</span>
                                                                                            <span class="anual-moduleprice">$228 Billed Annual</span>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1 / <span class="font-weight-900">1 Search</span></p>
                                                                                        <button type="button" class="btn module-upgrade">Upgrade Now!</button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="mb-0 main-color text-center d-inline mr-2"> <i class="fas fa-check-circle"></i></p>
                                                                                        <p class="module-name d-inline"> MiniMe </p>

                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="module-usage text-capitalize">1000 / <span class="font-weight-900">1000 Clicks</span></p>

                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>