<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/emilverify.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5  mt-77" id="emilverifylistview">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h5 class="font-weight-600 text-capitalize">Document Clean 01</h5>
                        <p class="color-grey">Uploaded at December 6th, 2018, 11:42 PM</p>
                    </div>
                    <div class="col-md-6 mobile-center text-right">
                        <button class="btn linear-btn linear-btn-shadow" type="button"> <span class="mr-2"><i class="fas fa-download"></i></span> download clean list</button>
                    </div>
                </div>
                <div class="clean-detailview my-5">

                    <div class="box-shadow">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="chart-box text-center pt-4">
                                    <h5 class="font-weight-700 text-capitalize mb-0">monthly value</h5>
                                    <canvas id="chart-area" class="my-3"></canvas>
                                    <p class="lower-text text-uppercase">verification</p>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-6 border-left-right">
                                <div class="email-count text-center py-5">
                                    <img src="images/emailcount.svg" class="img-fluid" alt="">
                                    <p class="color-black font-weight-700 mt-4">169,039</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 border-custom-right">
                                <div class="domain-rogress text-center py-5 px-5">
                                    <div class="domains my-4">
                                        <div class="text-left mb-2">
                                            <p class="text-capitalize d-inline font-weight-700">domains</p>
                                            <p class="text-right d-inline float-right mb-0 positive-activity"> 80%</p>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar positive-progress" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div class="suppressed my-4">
                                        <div class="text-left mb-2">
                                            <p class="text-capitalize  d-inline font-weight-700">suppressed</p>
                                            <p class="text-right d-inline float-right mb-0 negative-activity"> 25%</p>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar negative-progress" role="progressbar" style="width: 25%" aria-valuenow="25%" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="chartlabels-count text-center py-5 px-5">
                                    <p class="verified text-capitalize">
                                        <span>18395</span>Verified
                                    </p>
                                    <p class="undeliverable text-capitalize">
                                        <span>4748</span>Undeliverable
                                    </p>
                                    <p class="role text-capitalize">
                                        <span>4564</span>Role
                                    </p>
                                    <p class="unknown text-capitalize">
                                        <span>8798</span>Unknown
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/Chart.js"></script>

    <script>
        Chart.pluginService.register({
            beforeDraw: function(chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Lato';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#453937';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                        //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        400,
                        300,
                        800,
                        900,
                    ],

                    backgroundColor: [
                        "#02CED1",
                        "#FE7682",
                        "#7DD024",
                        "#F6EB53"
                    ],
                }],
                labels: [
                    'Verified',
                    'Undeliverable',
                    'Role',
                    'Unknown',
                ]
            },
            options: {
                responsive: true,
                legend: false,
                cutoutPercentage: 85,


                elements: {
                    center: {
                        text: '30,931',
                        color: '#453937', // Default is #453937
                        fontStyle: 'Lato', // Default is Lato
                        sidePadding: 50 // Defualt is 20 (as a percentage)
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
            }
        };
        var ctx = document.getElementById("chart-area").getContext("2d");
        var myChart = new Chart(ctx, config);
        myChart.data.datasets[0].data[2] = 800;
        myChart.update();
    </script>

</body>

</html>