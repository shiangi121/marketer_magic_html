<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/planupgrade.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <title>Marketer Magic</title>

</head>

<body>
    <header id="landing-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <a class="" href="#">
                        <img src="images/full-logo-black.svg" class="img-fluid" alt="">
                    </a>
                </div>
                <div class="col-md-6 text-right">
                    <button class="member-login text-capitalize">
                        member login
                    </button>
                </div>
            </div>
        </div>
    </header>
    <section class="header-line padding-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="mb-0 color-white"> <span class="font-weight-700">MarketerMagic</span> - Taking Your Business to a New Dimension of Marketing</p>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="font-weight-700">Join MarketerMagic For <span class="freetext">FREE</span> Today!</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="landing-form my-4">
                        <div class="form-group">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">Almost Complete…</div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <img src="images/landing-CP.svg" class="img-fluid mx-3" alt="">
                            <img src="images/landing-WH.svg" class="img-fluid mx-3" alt="">
                            <img src="images/landing-EV.svg" class="img-fluid mx-3" alt="">
                        </div>
                        <div class="form-group text-center py-3 border-bottom">
                            <h2 class="font-weight-700 text-capitalize">Start Your Free Trial Now!</h2>
                            <p class="font-weight-600">No contracts, downgrade or cancel your account anytime.</p>
                        </div>
                        <div class="form-group text-center py-3 mb-0" style="position:relative;">
                            <h3 class="font-weight-700 text-capitalize">Create Your Account Now</h3>
                            <p class="font-weight-600"> <span class="font-weight-700">Step #1 of 2:</span> Enter your Email & create a password...</p>
                            <img src="images/getstarted.svg" class="img-fluid get-started-img" alt="">
                        </div>
                        <div class="form-group px-5">
                            <label for="FirstName">First Name</label>
                            <input type="text" class="form-control" id="FirstName" placeholder="Name">
                        </div>
                        <div class="form-group px-5">
                            <label for="InputEmail">Email address</label>
                            <input type="email" class="form-control" id="InputEmail" placeholder="your@email.com">
                        </div>
                        <div class="form-group px-5">
                            <label for="InputPassword">Password</label>
                            <input type="password" class="form-control" id="InputPassword" placeholder="Enter your password">
                        </div>
                        <div class="px-5 border-bottom">
                            <button type="button" class="btn grediant-btn w-100" onclick="window.location.href='orderpage_step2.php'">get started</button>
                        </div>
                        <div class="customer-success px-5 padding-30">
                            <p class="heading">customer success</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="images/userimg.png" alt="">
                                </div>
                                <div class="col-md-9">
                                    <p class="mb-0"><em>“With so many tools and software in the FBA space, MarketerMagic is by far my favorite. It is my go-to tool for everything from product research to keyword optimization.” </em> <span class="font-weight-700">-  Sherly Edmunds</span></p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer id="landing-footer">
        <div class="container">
            <div class="col-md-12 text-center">
                <ul>
                    <li><a href="#">Terms Of Service</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">DMNCA Policy</a></li>
                    <li><a href="#">Income Disclosure</a></li>
                    <li><a href="#">Affiliates</a></li>
                </ul>
                <p class="mb-0">Yes, This Page Was Built Using MarketerMagic. Unlike Our Competitors, We drink our own Kool-Aid. <br> MarketerMagic.com - an Etison Product - All Rights Reserved @ 2018 - 2020 <br> 4410 Adair Street, San Diego, California 92107</p>
            </div>
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>