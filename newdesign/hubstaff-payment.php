<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaffpayment">
                <div class="row">
                    <div class="col-md-4 mobile-center">
                        <h5 class="font-weight-600">2 of 2 Payment Accounts</h5>
                        <div class="showselection d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm">
                                <option selected>All Status</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8 text-right mobile-center">
                        <button class="btn cancel-btn mr-3" type="button" onclick="window.location.href='hubstaff-sendpayment.php'">Manage payment accounts</button>
                        <button class="btn linear-btn linear-btn-shadow" type="button">Add member to payroll</button>
                    </div>
                </div>
                <div class="row py-5">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col" class="wd-20">name</th>
                                <th scope="col" class="wd-10">getway</th>
                                <th scope="col" class="wd-10">account</th>
                                <th scope="col">method</th>
                                <th scope="col" class="wd-10">periode</th>
                                <th scope="col">rate type</th>
                                <th scope="col">rate</th>
                                <th scope="col">status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <div class="member-info">
                                        <div class="float-left mr-3">
                                            <img src="images/userimg.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="member-name">
                                            <a href="#" class="font-weight-600 mb-0 d-inline text-capitalize">john deo</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <img src="images/paypal-img.png" class="img-fluid" alt="">
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="payment-account mb-0">jen.smith@gmail.com</p>
                                </td>
                                <td>
                                    <p class="payment-method">
                                        Automatic
                                        <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                    </p>
                                </td>
                                <td>
                                    <p class="period text-capitalize mb-0">
                                        weekly <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                <i class="fas fa-info-circle"></i>
                                            </button>
                                    </p>
                                    <small>07/03/19 - 07/09/19</small>
                                </td>
                                <td>
                                    <p class="hourly-rate mb-0 text-capitalize d-inline">Hourly <button class="editme-button" type="button"><i class="fas fa-pen"></i></button></p>
                                </td>
                                <td>
                                    <p class="rate mb-0 text-capitalize d-inline">$10.00 /hr<button class="editme-button" type="button"><i class="fas fa-pen"></i></button></p>
                                </td>

                                <td>
                                    <p class="mb-0 status-active d-inline">ENABLED</p>
                                    <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">enable payroll</a>
                                            <a class="dropdown-item" href="#">edit payment account</a>
                                            <a class="dropdown-item" href="#">delete payment account</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="member-info">
                                        <div class="float-left mr-3">
                                            <img src="images/userimg.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="member-name">
                                            <a href="#" class="font-weight-600 mb-0 d-inline text-capitalize">john deo</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <img src="images/paypal-img.png" class="img-fluid" alt="">
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="payment-account mb-0">jen.smith@gmail.com</p>
                                </td>
                                <td>
                                    <p class="payment-method">
                                        Automatic
                                        <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                    </p>
                                </td>
                                <td>
                                    <p class="period text-capitalize mb-0">
                                        weekly <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                <i class="fas fa-info-circle"></i>
                                            </button>
                                    </p>
                                    <small>07/03/19 - 07/09/19</small>
                                </td>
                                <td>
                                    <p class="hourly-rate mb-0 text-capitalize d-inline">Hourly <button class="editme-button" type="button"><i class="fas fa-pen"></i></button></p>
                                </td>
                                <td>
                                    <p class="rate mb-0 text-capitalize d-inline">$10.00 /hr<button class="editme-button" type="button"><i class="fas fa-pen"></i></button></p>
                                </td>

                                <td>
                                    <p class="mb-0 status-active d-inline">ENABLED</p>
                                    <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">enable payroll</a>
                                            <a class="dropdown-item" href="#">edit payment account</a>
                                            <a class="dropdown-item" href="#">delete payment account</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>