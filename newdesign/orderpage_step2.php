<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/planupgrade.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <title>Marketer Magic</title>

</head>

<body>
    <header id="landing-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <a class="" href="#">
                        <img src="images/full-logo-black.svg" class="img-fluid" alt="">
                    </a>
                </div>
                <div class="col-md-6 text-right">
                    <button class="member-login text-capitalize">
                        member login
                    </button>
                </div>
            </div>
        </div>
    </header>
    <section class="header-line padding-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="mb-0 color-white"> <span class="font-weight-700">MarketerMagic</span> - Taking Your Business to a New Dimension of Marketing</p>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="font-weight-700">Join MarketerMagic For <span class="freetext">FREE</span> Today!</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="landing-form my-4 landing-form-step2">
                        <div class="form-group">
                            <div class="progress">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">Almost Complete…</div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <img src="images/landing-CP.svg" class="img-fluid mx-3" alt="">
                            <img src="images/landing-WH.svg" class="img-fluid mx-3" alt="">
                            <img src="images/landing-EV.svg" class="img-fluid mx-3" alt="">
                        </div>
                        <div class="form-group text-center py-3 border-bottom">
                            <h2 class="font-weight-700 text-capitalize">Start Your Free Trial Now!</h2>
                            <p class="font-weight-600">No contracts, downgrade or cancel your account anytime.</p>
                        </div>
                        <div class="form-group text-center py-3 mb-0" style="position:relative;">
                            <h4><span class="font-weight-700">Step 2 of 2:</span> Billing Information</h4>
                            <!-- <img src="images/getstarted.svg" class="img-fluid get-started-img" alt=""> -->
                        </div>
                        <div class="form-group px-5">
                            <input type="text" class="form-control" id="address" placeholder="Full Address…">
                        </div>
                        <div class="form-group px-5">
                            <input type="email" class="form-control" id="city" placeholder="City Name…">
                        </div>
                        <div class="form-row px-5 mb-3">
                            <div class="forn-group col-md-8">
                                <input type="password" class="form-control" id="InputPassword" placeholder="State / Province…">
                            </div>
                            <div class="forn-group col-md-4">
                                <input type="password" class="form-control" id="InputPassword" placeholder="Zip Code…">
                            </div>
                        </div>
                        <div class="form-group px-5">
                            <select class="custom-select" name="select_country" id="select_country">
                                <option>Select Country</option>                                                                        
                                <option>test</option>
                                <option>test</option>
                                <option>test</option>
                            </select>
                        </div>
                        <div class="border-top border-bottom py-3 my-3 card-details">
                            <div class="form-group px-5">
                                <div class="cvv-tooltip">
                                    <p class="text-right cvv-text mb-2">
                                        <i class="fas fa-question-circle"></i>
                                        <span class="pl-1">Why do we ask for your CC?</span>
                                    </p>
                                    <div class="tooltip-box padding-20">
                                        <p class="mb-0 color-white">We ask for your credit card to prevent interruption of MarketerMagic conversion optimization tools should you decide to keep your account active. Your credit card will NOT be charged anything right now, you can
                                            cancel your subscription at anytime. You will ONLY ever be charged for the individual apps you use - and only after your free trial credits have expired. If you decide MarketerMagic isn't for you, you can cancel
                                            anytime.
                                        </p>
                                    </div>
                                </div>
                                <input type="text" class="form-control cardnum" id="cardnumber" placeholder="Card Number">
                            </div>
                            <div class="form-row px-5 mb-3">
                                <div class="forn-group col-md-6">
                                    <input type="date" class="form-control cardexpdate" id="expdate" placeholder="Date">
                                </div>
                                <div class="forn-group col-md-6">
                                    <input type="text" class="form-control cardcvv" id="cvv" placeholder="CVV">
                                </div>
                            </div>
                        </div>
                        <div class="py-4">
                            <h5 class="font-weight-700 text-center">Your Order Summary</h5>
                            <div class="order-summary mx-5 my-3">
                                <div class="padding-10 border-bottom">
                                    <p class="mb-0 text-capitalize">Current Selection: <span class="font-weight-700">Baller (Annual)</span>
                                        <span class="float-right">$47.00/mo</span>
                                    </p>
                                </div>
                                <div class="padding-10">
                                    <p class="main-color text-capitalize mb-1 font-weight-700">Total: (12 Months x $47/m)
                                        <span class="float-right">$564.00/year</span>
                                    </p>
                                    <p class="color-grey mb-0" style="font-size:12px;">
                                        Next billing date: <span class="font-weight-700">November 12th, 2020</span>.
                                    </p>
                                </div>
                            </div>
                            <div class="form-group px-5 text-left my-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck">
                                    <label class="custom-control-label color-black" for="customCheck">I have read and agree to the <a href="#" class="main-color">Term of Services</a></label>
                                </div>
                                <div class="custom-control custom-checkbox mt-3">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label color-black" for="customCheck1">Click Here to Ensure Uninterrupted Service by Enabling Autobilling - If You Have More Monthly Visitors Than You Expected <span class="font-weight-600">(Recommended!)</span></label>
                                </div>
                            </div>
                            <div class="px-5">
                                <button type="button" onclick="window.location.href='orderpage_step3.php'" class="btn grediant-btn w-100">get started</button>
                            </div>
                            <div class="form-group text-center">
                                <img src="images/Bitmap.svg" class="img-fluid mx-3" alt="">
                                <img src="images/stripe.svg" class="img-fluid mx-3" alt="">
                            </div>
                        </div>
                    </form>
                    <div class="customer-success px-5 padding-30 customer-success-step2">
                        <ul class="register-info mb-5">
                            <li>Your Card Will NOT be Charged After Your Free Trial Credits Expire – If You Decide That MarketerMagic Isn’t for You Your Card Will NEVER be Automatically Billed!</li>
                            <li>
                                Try all the Tools in our Marketing Suite Completely for Free!</li>
                            <li>Increase Conversions in Minutes!</li>
                            <li>Transcend to a New Dimension of Marketing!</li>
                        </ul>
                        <p class="heading">customer success</p>
                        <div class="row border-bottom py-4">
                            <div class="col-md-3">
                                <img src="images/userimg1.png" alt="">
                            </div>
                            <div class="col-md-9">
                                <p class="mb-0"><em>“With so many tools and software in the FBA space, MarketerMagic is by far my favorite. It is my go-to tool for everything from product research to keyword optimization.” </em> <span class="font-weight-700">-  Sherly Edmunds</span></p>
                            </div>
                        </div>
                        <div class="row border-bottom py-4">
                            <div class="col-md-3">
                                <img src="images/userimg2.png" alt="">
                            </div>
                            <div class="col-md-9">
                                <p class="mb-0"><em>“Lorem Ipsum has been the industry's standard standard dummy dummy text ever since the 1500s, when an unknown printer.”  
                                        </em> <span class="font-weight-700">-  Alan Smith</span></p>
                            </div>
                        </div>
                        <div class="row border-bottom py-4">
                            <div class="col-md-3">
                                <img src="images/userimg3.png" alt="">
                            </div>
                            <div class="col-md-9">
                                <p class="mb-0"><em>“Lorem Ipsum has been the industry's standard dummy dummy dummy text ever since the 1500s, when an unknown printer.”   </em> <span class="font-weight-700">-  Sherly Edmunds</span></p>
                            </div>
                        </div>
                        <div class="row py-4">
                            <div class="col-md-3">
                                <img src="images/userimg4.png" alt="">
                            </div>
                            <div class="col-md-9">
                                <p class="mb-0"><em>“Lorem Ipsum has been the industry's standard dummy dummy dummy text ever since the 1500s, when an unknown printer.”   </em> <span class="font-weight-700">-  John Smith</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer id="landing-footer">
        <div class="container">
            <div class="col-md-12 text-center">
                <ul>
                    <li><a href="#">Terms Of Service</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">DMNCA Policy</a></li>
                    <li><a href="#">Income Disclosure</a></li>
                    <li><a href="#">Affiliates</a></li>
                </ul>
                <p class="mb-0">Yes, This Page Was Built Using MarketerMagic. Unlike Our Competitors, We drink our own Kool-Aid. <br> MarketerMagic.com - an Etison Product - All Rights Reserved @ 2018 - 2020 <br> 4410 Adair Street, San Diego, California 92107</p>
            </div>
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>