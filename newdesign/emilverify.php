<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/dropzone.css">
    <link rel="stylesheet" href="css/emilverify.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5  mt-77" id="emilverify">
                <div class="row">
                    <div class="col-md-12 mobile-center">
                        <h5 class="font-weight-600 text-capitalize">my lists</h5>
                    </div>
                </div>
                <div class="row py-5 text-center">
                    <div class="col-md-12">
                        <form action="/file-upload" class="custom-dropzone">
                            <div class="dropzone upload-csv-dropzone row dropzone-mini">
                                <div class="fallback">
                                    <input type="file" name="file" />
                                </div>
                            </div>
                            <div id="previews" class="container mt-3"></div>
                        </form>
                        <p class="text-uppercase font-weight-700 color-black mt-4">or</p>
                        <button type="button" onclick="window.location.href='emilverify-cleanlistview.php'" class="btn linear-btn linear-btn-shadow">verify your first list</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mobile-center">
                        <h5 class="mb-3 font-weight-600 text-capitalize">imported lists</h5>
                        <div class="padding-20 imported-emaillist">
                            <ul>
                                <li class="padding-10 mb-3">
                                    <div class="row">
                                        <div class="col-lg-1 text-center">
                                            <img src="images/file-svg.svg" class="img-fluid" alt="">
                                            <img src="images/cleanedsvg.svg" class="img-fluid status-img" alt="">
                                        </div>
                                        <div class="col-lg-6 mt-4">
                                            <p class="mb-0">Data.2018.csv <b>(10947 records)</b></p>
                                            <p class="color-grey text-capitalize mb-0">Cleaned</p>
                                        </div>
                                        <div class="col-lg-5 text-right mt-4">
                                            <button type="button" class="btn linear-btn linear-btn-shadow"> <span><i class="fas fa-download"></i></span> download clean list</button>
                                            <button class="btn color-grey ml-3" type="button"><i class="fas fa-ellipsis-v"></i></button>
                                        </div>
                                    </div>
                                </li>
                                <li class="padding-10 mb-3">
                                    <div class="row">
                                        <div class="col-lg-1 text-center">
                                            <img src="images/file-svg.svg" class="img-fluid" alt="">
                                            <img src="images/unprocessedsvg.svg" class="img-fluid status-img" alt="">
                                        </div>
                                        <div class="col-lg-6 mt-4">
                                            <p class="mb-0">Data.2018.csv <b>(10947 records)</b></p>
                                            <p class="color-grey text-capitalize mb-0">Unprocessed</p>
                                        </div>
                                        <div class="col-lg-5 text-right mt-4">
                                            <div class="padding-20 d-inline">
                                                <a href="#" class="text-capitalize main-color">start list <img src="images/right-arrow.svg" class="img-fluid ml-3" alt=""></a>
                                            </div>
                                            <button class="btn color-grey ml-3" type="button"><i class="fas fa-ellipsis-v"></i></button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/dropzone.js"></script>
    <script src="js/Chart.js"></script>

    <script>
        Dropzone.autoDiscover = false;
        var cv_dropzone = $(".upload-csv-dropzone").dropzone({
            url: 'https://mm.avdevs.com/store_ev_campaign_csv',
            autoProcessQueue: false,
            addRemoveLinks: true,
            uploadMultiple: true,
            parallelUploads: 100,
            previewsContainer: "#previews",
            dictDefaultMessage: "<label class=\"uploadfile-label\" for=\"uploadcsv\">\n" +
                "<p class=\"upload-fileicon\"><i class=\"fas fa-file-alt\"></i></p>\n" +
                " <h5 class=\"color-black text-capitalize mb-0\">drop list here</h5>\n" +
                "<p class=\"main-color text-uppercase font-weight-700 mb-0\">or click to upload</p>\n" +
                "<p class=\"color-grey mb-0\">(CSV file only)</p>\n" +
                "</label>",
        });
    </script>
</body>

</html>