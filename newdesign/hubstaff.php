<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link href="css/lightgallery.css" rel="stylesheet">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaff">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h5 class="font-weight-600">WorkHub (Mon, Mar 11, 2019 - Sun, Mar 17, 2019)</h5>
                        <div class="showselection d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm">
                                <option>Team</option>
                                <option selected>Me</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <div class="dropdown d-inline mx-3 workhubextensiondropdown">
                            <button class="btn padding-0 dropdown-toggle" type="button" id="workhubextension" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="images/workhubextension.svg" alt="" height="40px"> 
                            </button>
                            <div class="dropdown-menu padding-0" aria-labelledby="workhubextension">
                                <div class="padding-10 dropdown-header">
                                    <div class="header-logo d-inline">
                                        <img src="images/workhubextensionlogo.png" height="40px" alt="">
                                    </div>
                                    <div class="header-settings float-right">
                                        <a href="#"><i class="fas fa-cog"></i></a>
                                    </div>
                                </div>
                                <div class="padding-10 dropdown-content text-center">
                                    <p>total worked today
                                        <span> 
                                            <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                <i class="fas fa-question-circle"></i>
                                            </button>
                                        </span>
                                    </p>
                                    <h5 class="work-hours">
                                        06:21
                                    </h5>
                                    <form action="">
                                        <div class="form-group">
                                            <label for="project" class="w-100 text-left">project </label>
                                            <select required="" class="custom-select" id="project">
                                                <option selected="" value="MarketerMagics">MarketerMagics</option>
                                                <option value="1">ABC</option>
                                                <option value="2">XYZ</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="task" class="w-100 text-left">task </label>
                                            <select required="" class="custom-select" id="task">
                                                <option selected="" value="Select a task">Select a task</option>
                                                <option value="1">ABC</option>
                                                <option value="2">XYZ</option>
                                            </select>
                                        </div>
                                        <button class="start-timer w-100 btn">start timer</button>
                                    </form>
                                </div>
                                <div class="padding-10 dropdown-footer border-top">
                                    <div class="edit d-inline">
                                        <a href="#"><i class="fas fa-pen"></i></a>
                                    </div>
                                    <div class="last-updated float-right">

                                        <p> <span class="mr-2"><i class="fas fa-sync-alt"></i></span>Last updated at: 02/27/19 11:31:47</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn linear-btn linear-btn-shadow" type="button"><img src="images/widget.svg" class="img-fluid pr-3" alt="">Manage Widget</button>
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="col-md-3">
                        <div class="box-shadow activity-box">
                            <div class="padding-20 pb-0">
                                <div class="title d-inline">
                                    <div class="bg-yellow padding-10 mr-1">
                                        <img src="images/activity.svg" class="img-fluid" alt="">
                                    </div>
                                    <h5 class="text-capitalize font-weight-600 d-inline">activity</h5>
                                </div>
                                <div class="float-right color-grey"><i class="fas fa-ellipsis-v"></i></div>
                            </div>
                            <div class="padding-20">
                                <div class="number">
                                    <p class="mb-0 d-inline">75%</p>
                                    <img src="images/graph.png" class="img-fluid float-right" alt="">
                                </div>
                                <div class="percount">
                                    <div class="positive-per">
                                        <p class="mb-0"><i class="fas fa-caret-up"></i> 10%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="createwidg text-center">
                            <a href="#"><img src="images/mountains.png" class="img-fluid mb-3" alt="">
                                <p class="main-color">Add new widget</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="col-md-12 col-lg-6">
                        <div class="box-shadow recent-activity">
                            <div class="padding-20 pb-0">
                                <div class="title d-inline">
                                    <div class="bg-green padding-10 mr-1">
                                        <img src="images/watchicon.svg" style="height:20px;" class="img-fluid" alt="">
                                    </div>
                                    <h5 class="text-capitalize font-weight-600 d-inline"> recent activity</h5>
                                </div>
                                <div class="float-right color-grey"><i class="fas fa-ellipsis-v"></i></div>
                            </div>
                            <div class="padding-20">
                                <div class="gallery padding-20">
                                    <ul id="lightgallery" class="list-unstyled row">
                                        <li class="col-xs-6 col-sm-4 col-md-4" data-src="images/lightbox/activity-img.png">
                                            <div class="activityper">
                                                <div class="positive-activity">
                                                    <p class="mb-0">75%</p>
                                                </div>
                                            </div>
                                            <a href="" class="on-hover">
                                                <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                <div class="hover-box">
                                                    <p class="view-screen">view screen</p>
                                                </div>
                                            </a>

                                        </li>
                                        <li class="col-xs-6 col-sm-4 col-md-4" data-src="images/lightbox/activity-img.png">
                                            <div class="activityper">
                                                <div class="average-activity">
                                                    <p class="mb-0">45%</p>
                                                </div>
                                            </div>
                                            <a href="" class="on-hover">
                                                <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                <div class="hover-box">
                                                    <p class="view-screen">view screen</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="col-xs-6 col-sm-4 col-md-4" data-src="images/lightbox/activity-img.png">
                                            <div class="activityper">
                                                <div class="negative-activity">
                                                    <p class="mb-0">19%</p>
                                                </div>
                                            </div>
                                            <a href="" class="on-hover">
                                                <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                <div class="hover-box">
                                                    <p class="view-screen">view screen</p>
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="padding-20 text-center border-top">
                                <a href="hubstaff-activity.php" class="text-capitalize color-black">view activity <span class="ml-2"><i class="fas fa-arrow-right"></i></span> </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="box-shadow projects-list">
                            <div class="padding-20 pb-0">
                                <div class="title d-inline">
                                    <div class="bg-blue padding-10 mr-1">
                                        <img src="images/project.svg" style="height:20px;" class="img-fluid" alt="">
                                    </div>
                                    <h5 class="text-capitalize font-weight-600 d-inline"> projects</h5>
                                </div>
                                <div class="float-right color-grey"><i class="fas fa-ellipsis-v"></i></div>
                            </div>
                            <div class="padding-20">
                                <div class="project-info">
                                    <div class="float-left mr-3">
                                        <div class="project-initial bg-lightblue">
                                            <p class="text-capitalize mb-0">t</p>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="project-name d-inline">
                                            <a href="#" class="font-weight-600">TLN Team</a>
                                        </div>
                                        <div class="project-time-activity float-right d-flex">
                                            <p class="mb-0 time"><i class="fas fa-history"></i> <span>27:30:07</span></p>
                                            <div class="negative-activity">
                                                <p class="mb-0">19%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar negative-progress" role="progressbar" style="width: 19%;" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="padding-20">
                                <div class="project-info">
                                    <div class="float-left mr-3">
                                        <div class="project-initial bg-lightred">
                                            <p class="text-capitalize mb-0">n</p>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="project-name d-inline">
                                            <a href="#" class="font-weight-600">Ninja Team</a>
                                        </div>
                                        <div class="project-time-activity float-right d-flex">
                                            <p class="mb-0 time"><i class="fas fa-history"></i> <span>27:30:07</span></p>
                                            <div class="positive-activity">
                                                <p class="mb-0">80%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar positive-progress" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="padding-20">
                                <div class="project-info">
                                    <div class="float-left mr-3">
                                        <div class="project-initial bg-darkgrey">
                                            <p class="text-capitalize mb-0">d</p>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="project-name d-inline">
                                            <a href="#" class="font-weight-600">Development Team</a>
                                        </div>
                                        <div class="project-time-activity float-right d-flex">
                                            <p class="mb-0 time"><i class="fas fa-history"></i> <span>27:30:07</span></p>
                                            <div class="average-activity">
                                                <p class="mb-0">50%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar average-progress" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="padding-20 text-center border-top">
                                <a href="hubstaff-project.php" class="text-capitalize color-black">view reports <span class="ml-2"><i class="fas fa-arrow-right"></i></span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="col-md-12 col-lg-6">
                        <div class="timesheet box-shadow">
                            <div class="padding-20 pb-0">
                                <div class="title d-inline">
                                    <div class="bg-red padding-10 mr-1">
                                        <img src="images/usersicon.svg" style="height:20px;" class="img-fluid" alt="">
                                    </div>
                                    <h5 class="text-capitalize font-weight-600 d-inline"> Timesheet</h5>
                                </div>
                                <div class="float-right color-grey"><i class="fas fa-ellipsis-v"></i></div>
                            </div>
                            <div class="padding-20">
                                <div class="track text-center">
                                    <a href="#" data-toggle="modal" data-target="#createcampaignmodal"><img src="images/mountains.png" class="img-fluid mb-3" alt="">
                                        <p class="main-color">Track applications & sites</p>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#lightgallery').lightGallery();
        });
    </script>
    <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
    <script src="js/lightgallery-all.min.js"></script>
    <script src="js/jquery.mousewheel.min.js"></script>
</body>

</html>