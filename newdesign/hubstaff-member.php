<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaffmembers">
                <div class="row">
                    <div class="col-md-3 mobile-center">
                        <h5 class="font-weight-600">Member (5)</h5>
                        <div class="showselection d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm">
                                <option selected>Members</option>
                                <option>Invities</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-9 text-right mobile-center">
                        <div class="dropdown d-inline batch-actions">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    batch actions
                                </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit pay rate</a>
                                <a class="dropdown-item" href="#">Edit bill rate</a>
                                <a class="dropdown-item" href="#">Manage weekly limit</a>
                                <a class="dropdown-item" href="#">Disable time tracking</a>
                                <a class="dropdown-item" href="#">Remove member</a>
                            </div>
                        </div>
                        <form class="form-inline d-inline search-project">
                            <i class="fas fa-search"></i>
                            <input class="form-control" type="search" placeholder="Search members" aria-label="Search">
                        </form>
                        <button class="btn linear-btn linear-btn-shadow" data-toggle="modal" data-target="#invitemembersModal" type="button">invite member</button>
                    </div>
                </div>
                <div class="row py-5">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1"></label>
                                    </div>
                                </th>
                                <th scope="col" class="wd-25">member</th>
                                <th scope="col">role</th>
                                <th scope="col" class="wd-10">peoject</th>
                                <th scope="col" class="wd-25">payment</th>
                                <th scope="col" class="wd-20">weekly limit</th>
                                <th scope="col">time tracking</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="member-info">
                                        <div class="float-left mr-3">
                                            <div class="member-initial bg-lightblue">
                                                <p class="text-capitalize mb-0">j</p>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="member-name">
                                                <a href="hubstaff-memberdetails.php" class="font-weight-600 mb-0 d-inline">john deo</a>
                                                <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                    <i class="fas fa-question-circle"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <select class="custom-select role-dropdown">
                                        <option selected>User</option>
                                        <option value="1">Developer</option>
                                        <option value="2">Designer</option>
                                    </select>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline">1 </p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline color-grey">No pay rate / No bill rate</p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline color-grey">No weekly limit</p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 status-active">enabled</p>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" data-toggle="modal" data-target="#editmemberModal" href="#">edit peoject membership</a>
                                            <a class="dropdown-item" data-toggle="modal" data-target="#editpayrateModal" href="#">edit pay rates</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label" for="customCheck3"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="member-info">
                                        <div class="float-left mr-3">
                                            <div class="member-initial bg-lightred">
                                                <p class="text-capitalize mb-0">k</p>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="member-name">
                                                <a href="#" class="font-weight-600 mb-0 d-inline">kevin</a>
                                                <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                    <i class="fas fa-question-circle"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <select class="custom-select role-dropdown">
                                        <option selected>User</option>
                                        <option value="1">Developer</option>
                                        <option value="2">Designer</option>
                                    </select>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline">1 </p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline color-grey">Hourly: <b class="color-black">$2.36</b> / No bill rate</p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline color-grey">No weekly limit</p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 status-active">enabled</p>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">edit peoject membership</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                                        <label class="custom-control-label" for="customCheck4"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="member-info">
                                        <div class="float-left mr-3">
                                            <div class="member-initial bg-lightred">
                                                <p class="text-capitalize mb-0">k</p>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="member-name">
                                                <a href="#" class="font-weight-600 mb-0 d-inline">kevin</a>
                                                <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                    <i class="fas fa-question-circle"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <select class="custom-select role-dropdown">
                                        <option selected>User</option>
                                        <option value="1">Developer</option>
                                        <option value="2">Designer</option>
                                    </select>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline">1 </p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline color-grey">No pay rate / No bill rate</p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline color-grey">No weekly limit</p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="mb-0 status-active">enabled</p>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">edit peoject membership</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>

    <!-- Invite Member Modal -->
    <div class="modal fade" id="invitemembersModal" tabindex="-1" role="dialog" aria-labelledby="invitemembersModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header padding-20">
                    <div class="float-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="invitemembersModalTitle">Invite member</h5>
                </div>
                <div class="modal-body padding-20">
                    <form action="">
                        <div class="form-group">
                            <label for="email" class="w-100">email <a href="#" class="float-right main-color">Copy invite link</a></label>
                            <input required type="email" class="form-control" id="email" aria-describedby="emailhelp" placeholder="Enter email addresses separated by new lines">
                        </div>
                        <div class="form-group">
                            <label for="role" class="w-100">role <a href="#" class="float-right main-color">Learn more</a></label>
                            <select required class="custom-select" id="role">
                                <option selected>User</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="projects" class="w-100">projects <a href="#" class="float-right main-color">Learn more</a></label>
                            <select required class="custom-select" id="projects">
                                <option selected>Select Project</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer padding-20">
                    <button type="button" class="btn cancel-btn mb-3" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn linear-btn linear-btn-shadow mb-3">Send </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit project memberships Modal -->
    <div class="modal fade" id="editmemberModal" tabindex="-1" role="dialog" aria-labelledby="editmemberModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header padding-20">
                    <div class="float-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="editmemberModalTitle">Edit project memberships</h5>
                </div>
                <div class="modal-body padding-20">
                    <form action="">
                        <div class="form-group">
                            <p class="heading-text">member</p>
                            <div class="member-info">
                                <div class="float-left mr-2">
                                    <div class="member-initial bg-lightblue" style="font-size: 12px; padding: 5px 12px;">
                                        <p class="text-capitalize mb-0">j</p>
                                    </div>
                                </div>
                                <div class="mb-2">
                                    <div class="member-name">
                                        <p class="color-grey mb-0 d-inline text-capitalize">john deo</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="heading-text">manager</p>
                            <label class="w-100" for="manager" style="font-size:12px;">Oversees and manages the project<a href="#" class="float-right main-color">select all</a></label>
                            <select class="custom-select" id="manager" aria-describedby="managerhelp">
                                <option selected>Select Manager</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <p class="heading-text">user</p>
                            <label class="w-100" for="user" style="font-size:12px;">Works on the project, will not see other users (most common)<a href="#" class="float-right main-color">select all</a></label>
                            <input type="text" value="Videos and Graphics" data-role="tagsinput" id="tags" class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <p class="heading-text">viewer</p>
                            <label class="w-100" for="viewer" style="font-size:12px;">Can view team reports for this project<a href="#" class="float-right main-color">select all</a></label>
                            <select class="custom-select" id="viewer" aria-describedby="viewerhelp">
                                <option selected>Select Viewer</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer padding-20">
                    <button type="button" class="btn cancel-btn mb-3" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn linear-btn linear-btn-shadow mb-3">Save </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit pay Rate  Modal -->
    <div class="modal fade" id="editpayrateModal" tabindex="-1" role="dialog" aria-labelledby="editpayrateModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header padding-20">
                    <div class="float-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="invitemembersModalTitle">Edit pay rates (1 member)</h5>
                </div>
                <div class="modal-body padding-20">
                    <form action="">
                        <div class="form-group">
                            <label for="name">Organization</label>
                            <p class="orgo-name color-grey">THATLifestyleNinja</p>
                        </div>
                        <div class="form-group">
                            <label for="role" class="w-100">pay rate</label>
                            <select required class="custom-select" id="role" style="width:80%;">
                                <option selected>$100 - $200</option>
                                <option value="1">$200 - $500</option>
                            </select>
                            <p class="color-grey d-inline ml-3">USD /hr</p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer padding-20">
                    <button type="button" class="btn cancel-btn mb-3" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn linear-btn linear-btn-shadow mb-3">Save </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/bootstrap-tagsinput.js"></script>
</body>

</html>