<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/emilverify.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5  mt-77" id="emilverifylistview">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h5 class="font-weight-600 text-capitalize">my lists</h5>
                    </div>
                    <div class="col-md-6 mobile-center text-right">
                        <button class="btn linear-btn linear-btn-shadow" type="button"> <span class="mr-2"><img src="images/right.svg" class="img-fluid" alt=""></span> verify new list</button>
                    </div>
                </div>
                <div class="row clean-listview my-5">
                    <div class="col-lg-4 col-md-12">
                        <div class="box-shadow">
                            <div class="padding-10">
                                <div class="d-inline">
                                    <a href="#" class="color-grey"><i class="fas fa-tv"></i></a>
                                </div>
                                <div class="float-right">
                                    <a href="#" class="color-grey"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </div>
                            <div class="text-center padding-10">
                                <h5 class="text-capitalize font-weight-600">Winter Newsletter</h5>
                                <p class="color-grey">Uploaded at 10/28/2018 - 6:01 am</p>
                                <div class="chart-box">
                                    <p class="upper-text text-capitalize">Verifying List</p>
                                    <canvas id="chart-area-verify" class="my-3"></canvas>
                                    <p class="lower-text text-uppercase">completed</p>
                                </div>
                            </div>
                            <div class="padding-20 text-center custom-light-bg">
                                <h6 class="font-weight-700 text-capitalize">Estimated Cost</h6>
                                <p class="main-color" style="font-size: 32px;">$164.41</p>
                                <button class="btn verify-btn w-100" type="button">verify list</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="box-shadow">
                            <div class="padding-10">
                                <div class="d-inline">
                                    <a href="#" class="color-grey"><i class="fas fa-tv"></i></a>
                                </div>
                                <div class="float-right">
                                    <a href="#" class="color-grey"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </div>
                            <div class="text-center padding-10">
                                <h5 class="text-capitalize font-weight-600">West Cost Leads</h5>
                                <p class="color-grey">Uploaded at 10/28/2018 - 6:01 am</p>
                                <div class="chart-box">
                                    <p class="upper-text text-capitalize">Complete</p>
                                    <canvas id="chart-area" class="my-3"></canvas>
                                    <p class="lower-text text-uppercase">verification</p>
                                </div>
                            </div>
                            <div class="padding-20 text-center custom-light-bg">
                                <h6 class="font-weight-700 text-capitalize">total Cost</h6>
                                <p class="main-color" style="font-size: 32px;">$199.65</p>
                                <button class="btn linear-btn w-100" onclick="window.location.href='emilverify-cleandetailview.php'" type="button">view details</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="box-shadow">
                            <div class="padding-10">
                                <div class="d-inline">
                                    <a href="#" class="color-grey"><i class="fas fa-tv"></i></a>
                                </div>
                                <div class="float-right">
                                    <a href="#" class="color-grey"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </div>
                            <div class="text-center padding-10">
                                <h5 class="text-capitalize font-weight-600">Fall Campaign</h5>
                                <p class="color-grey">Uploaded at 10/28/2018 - 6:01 am</p>
                                <div class="chart-box">
                                    <p class="upper-text text-capitalize">Complete</p>
                                    <canvas id="chart-area1" class="my-3"></canvas>
                                    <p class="lower-text text-uppercase">verification</p>
                                </div>
                            </div>
                            <div class="padding-20 text-center custom-light-bg">
                                <h6 class="font-weight-700 text-capitalize">total Cost</h6>
                                <p class="main-color" style="font-size: 32px;">$444.65</p>
                                <button class="btn linear-btn w-100" type="button">view details</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/Chart.js"></script>
    <script>
        Chart.pluginService.register({
            beforeDraw: function(chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Lato';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#453937';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                        //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        1000,
                        900,
                    ],

                    backgroundColor: [
                        "#EDEDED",
                        "#02CED1",
                    ],
                }],
                labels: [
                    'Verified',
                    'Undeliverable',
                ]
            },
            options: {
                responsive: true,
                legend: false,
                cutoutPercentage: 85,


                elements: {
                    center: {
                        text: '45%',
                        color: '#453937 ', // Default is #453937
                        fontStyle: 'Lato', // Default is Lato
                        sidePadding: 50 // Defualt is 20 (as a percentage)
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
            }
        };
        var ctx = document.getElementById("chart-area-verify").getContext("2d");
        var myChart = new Chart(ctx, config);
        myChart.data.datasets[0].data[1] = 800;
        myChart.update();
    </script>
    <script>
        Chart.pluginService.register({
            beforeDraw: function(chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Lato';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#453937';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                        //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        400,
                        300,
                        800,
                        900,
                    ],

                    backgroundColor: [
                        "#02CED1",
                        "#FE7682",
                        "#7DD024",
                        "#F6EB53"
                    ],
                }],
                labels: [
                    'Verified',
                    'Undeliverable',
                    'Role',
                    'Unknown',
                ]
            },
            options: {
                responsive: true,
                legend: false,
                cutoutPercentage: 85,


                elements: {
                    center: {
                        text: '30,931',
                        color: '#453937', // Default is #453937
                        fontStyle: 'Lato', // Default is Lato
                        sidePadding: 50 // Defualt is 20 (as a percentage)
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
            }
        };
        var ctx = document.getElementById("chart-area").getContext("2d");
        var myChart = new Chart(ctx, config);
        myChart.data.datasets[0].data[2] = 800;
        myChart.update();
    </script>
    <script>
        Chart.pluginService.register({
            beforeDraw: function(chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Lato';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#453937';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                        //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        400,
                        300,
                        800,
                        900,
                    ],

                    backgroundColor: [
                        "#02CED1",
                        "#FE7682",
                        "#7DD024",
                        "#F6EB53"
                    ],
                }],
                labels: [
                    'Verified',
                    'Undeliverable',
                    'Role',
                    'Unknown',
                ]
            },
            options: {
                responsive: true,
                legend: false,
                cutoutPercentage: 85,


                elements: {
                    center: {
                        text: '50,931',
                        color: '#453937', // Default is #453937
                        fontStyle: 'Lato', // Default is Lato
                        sidePadding: 50 // Defualt is 20 (as a percentage)
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
            }
        };
        var ctx = document.getElementById("chart-area1").getContext("2d");
        var myChart = new Chart(ctx, config);
        myChart.data.datasets[0].data[2] = 800;
        myChart.update();
    </script>
</body>

</html>