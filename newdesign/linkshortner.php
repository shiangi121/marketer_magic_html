<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/urlshortner.css">

    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="clickproof">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h4 class="font-weight-500">Link Shortener</h4>
                        <p class="color-grey"> <span class="mr-2"><i class="fas fa-globe-americas"></i></span> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <div class="btn-group urlcreate">
                            <button type="button" data-toggle="modal" data-target="#createModal" class="btn linear-btn linear-btn-shadow"> create</button>
                            <button type="button" class="btn dropdown-toggle dropdown-toggle-split linear-btn linear-btn-shadow" id="create" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                            <div class="dropdown-menu create" aria-labelledby="create">
                                <a class="dropdown-item" href="#">Link Shortener</a>
                                <a class="dropdown-item" href="#">Link Splitter</a>
                                <a class="dropdown-item" href="#">Re-targeting Link</a>
                                <a class="dropdown-item" href="#">Deep Link</a>
                                <a class="dropdown-item" href="#">Link Tree</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-5">
                    <div class="col-lg-3 col-md-16 col-sm-12">
                        <div class="box-shadow">
                            <div class="padding-20">
                                <img src="images/link-blue.svg" alt="" class="mr-2">
                                <h6 class="text-capitalize font-weight-700 d-inline">Link Shortener</h6>
                                <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-v"></i></a> </p>
                            </div>
                            <div class="padding-20">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <p class="mb-2 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                        <h6 class="headingtxt mb-0">total clicks</h6>
                                    </div>
                                    <div class="col-lg-6 col-md-6 text-center">
                                        <img src="images/chart-blue.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="border-top padding-20">
                                <p class="mb-0">http://invis.io/5G1YULBCE</p>
                                <p class="mb-0 main-color" style="font-size:12px;">bit.ly/learningpagebit.ly/learning</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pb-5">
                    <h5 class="text-capitalize font-weight-600">Recent Short Links</h5>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col" class="wd-20">short link</th>
                                <th scope="col" class="wd-50">original link</th>
                                <th scope="col" class="wd-18">created at</th>
                                <th scope="col">total clicks</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <div class="copy"><a href="#"><i class="far fa-copy"></i></a></div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 main-color">bit.ly/learningpage</p>
                                </td>
                                <td>
                                    <p class="mb-0">https://www.behance.net/gallery/372…</p>
                                </td>
                                <td>
                                    <p class="mb-0">Jan 8, 2015, 9:48 am</p>
                                </td>
                                <td>
                                    <p class="mb-0 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="copy-edit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="copy-edit">
                                            <a class="dropdown-item" href="#">copy</a>
                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#EditModal">edit</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="copy"><a href="#"><i class="far fa-copy"></i></a></div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 main-color">bit.ly/learningpage</p>
                                </td>
                                <td>
                                    <p class="mb-0">https://www.behance.net/gallery/372…</p>
                                </td>
                                <td>
                                    <p class="mb-0">Jan 8, 2015, 9:48 am</p>
                                </td>
                                <td>
                                    <p class="mb-0 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="copy-edit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="copy-edit">
                                            <a class="dropdown-item" href="#">copy</a>
                                            <a class="dropdown-item" href="#">edit</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="copy"><a href="#"><i class="far fa-copy"></i></a></div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 main-color">bit.ly/learningpage</p>
                                </td>
                                <td>
                                    <p class="mb-0">https://www.behance.net/gallery/372…</p>
                                </td>
                                <td>
                                    <p class="mb-0">Jan 8, 2015, 9:48 am</p>
                                </td>
                                <td>
                                    <p class="mb-0 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="copy-edit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="copy-edit">
                                            <a class="dropdown-item" href="#">copy</a>
                                            <a class="dropdown-item" href="#">edit</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-md-12 show-more text-right mt-3">
                        <a href="#"> show more</a>
                    </div>

                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <div class="modal fade url-modal" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-right" role="document" style="position:unset">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="createModalTitle">Create Short Link</h5>
                    <p class="d-block">Lorem Ipsum has been the industry's standard dummy.</p>
                </div>
                <div class="modal-body padding-0">
                    <div class="padding-20">
                        <form class="my-2">
                            <label for="">Paste Long URL here: </label>
                            <input class="form-control" type="text" placeholder="http://www.google.com/">
                        </form>
                    </div>
                </div>
                <div class="modal-footer justify-content-start padding-20 border-top">
                    <button type="button" data-dismiss="modal" class="btn linear-btn w-100">create a link</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade url-modal" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="EditModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-right" role="document" style="position:unset">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="EditModalTitle">Edit Short Link</h5>
                    <p class="d-block">Lorem Ipsum has been the industry's standard dummy.</p>
                </div>
                <div class="modal-body padding-0">
                    <form class="my-2">
                        <div class="padding-20 form-group mb-0">
                            <p class="main-color mb-0">
                                mini.me/2Wtv2ll
                                <span class="float-right"><a href="#" class="copy-link"><i class="far fa-copy"></i> copy</a></span>
                            </p>
                        </div>
                        <div class="padding-20 border-top pb-0">
                            <p class="color-grey text-uppercase">
                                CREATED MAR 27
                                <span class="main-color float-right">
                                    <a href="#" class="text-capitalize">Hide Link</a>
                                </span>
                            </p>
                            <div class="form-group">
                                <label for="">Title </label>
                                <input class="form-control" type="text" placeholder="Title Campaign">
                            </div>
                            <div class="form-group">
                                <label for="">Customize </label>
                                <input class="form-control" type="text" placeholder="mini.me/2Wtv2ll">
                            </div>
                        </div>
                        <div class="padding-20 utm-parameters">
                            <a class="btn collapselink color-grey text-uppercase" data-toggle="collapse" href="#UTMPara" role="button" aria-expanded="true" aria-controls="UTMPara">
                                    Add UTM Parameters (Optional) <i class="fas fa-angle-down"></i> 
                                    </a>
                            <div class="collapse show" id="UTMPara">
                                <div class="card card-body border-0 rounded-0 padding-0 UTMParacollapse mt-3">
                                    <div class="form-group">
                                        <label for="">Traffic Source <span class="color-red">*</span> </label>
                                        <select class="custom-select">
                                            <option selected>Google</option>
                                            <option value="1">abc</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Paid or Organic </label>
                                        <select class="custom-select">
                                            <option selected>Paid Social Media</option>
                                            <option value="1">xyz</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Unique Identifier </label>
                                        <input class="form-control" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-start padding-20 border-top">
                    <button type="button" data-dismiss="modal" class="btn linear-btn w-100">save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>