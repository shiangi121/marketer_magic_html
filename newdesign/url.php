<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/urlshortner.css">

    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="clickproof">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h4 class="font-weight-500">MiniMe</h4>
                        <p class="color-grey">The Worlds Most Powerful Links</p>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <div class="btn-group urlcreate">
                            <button type="button" class="btn linear-btn linear-btn-shadow"> create</button>
                            <button type="button" class="btn dropdown-toggle dropdown-toggle-split linear-btn linear-btn-shadow" id="create" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                            <div class="dropdown-menu create" aria-labelledby="create">
                                <a class="dropdown-item" href="#">Link Shortener</a>
                                <a class="dropdown-item" href="#">Link Splitter</a>
                                <a class="dropdown-item" href="#">Re-targeting Link</a>
                                <a class="dropdown-item" href="#">Deep Link</a>
                                <a class="dropdown-item" href="#">Link Tree</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-5">
                    <div class="col-lg-6 col-md-12">
                        <div class="box-shadow">
                            <div class="padding-20">
                                <img src="images/link-red.svg" alt="" class="mr-2">
                                <h6 class="text-capitalize font-weight-700 d-inline">Link Splitter</h6>
                                <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-h"></i></a> </p>
                            </div>
                            <div class="range-slider padding-20">
                                <input type="range" step="10" min="0" max="100" value="50" class="slider" id="percentage">
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        <p class="font-weight-700 mb-0" id="first"></p>
                                        <p class="color-grey">https://marketermagic.com/splitUrl </p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <p class="font-weight-700 mb-0" id="second"></p>
                                        <p class="color-grey">https://marketermagic.com/splitUrl </p>
                                    </div>
                                </div>
                            </div>
                            <div class="padding-20 border-top">
                                <h6 class="headingtxt mb-3">
                                    total conversions
                                </h6>
                                <div class="row mb-3">
                                    <div class="col-lg-8 col-md-6">
                                        <p class="mb-2 main-color text-uppercase font-weight-600"> <span class="circle color-orange"><i class="fas fa-circle"></i></span> url A :</p>
                                        <p class="mb-2 ml-3" style="color: #5F6466;">https://marketermagic.com/splitUrl</p>
                                        <p class="mb-2 ml-3 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                    </div>
                                    <div class="col-lg-4 col-md-6 text-center">
                                        <img src="images/chart-black.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-lg-8 col-md-6">
                                        <p class="mb-2 main-color text-uppercase font-weight-600"> <span class="circle color-black"><i class="fas fa-circle"></i></span> url b :</p>
                                        <p class="mb-2 ml-3" style="color: #5F6466;">https://marketermagic.com/splitUrl</p>
                                        <p class="mb-2 ml-3 font-weight-600">89 <img src="images/chartsvg.svg" alt=""> </p>
                                    </div>
                                    <div class="col-lg-4 col-md-6 text-center">
                                        <img src="images/chart-yellow.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-shadow">
                                    <div class="padding-20">
                                        <img src="images/link-blue.svg" alt="" class="mr-2">
                                        <h6 class="text-capitalize font-weight-700 d-inline">Link Shortener</h6>
                                        <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-v"></i></a> </p>
                                    </div>
                                    <div class="padding-20">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <p class="mb-2 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                                <h6 class="headingtxt mb-0">total clicks</h6>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-center">
                                                <img src="images/chart-blue.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-top padding-20">
                                        <p class="mb-0">http://invis.io/5G1YULBCE</p>
                                        <p class="mb-0 main-color" style="font-size:12px;">bit.ly/learningpagebit.ly/learning</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-shadow">
                                    <div class="padding-20">
                                        <img src="images/link-green.svg" alt="" class="mr-2">
                                        <h6 class="text-capitalize font-weight-700 d-inline">Deep Link</h6>
                                        <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-v"></i></a> </p>
                                    </div>
                                    <div class="padding-20">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <p class="mb-2 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                                <h6 class="headingtxt mb-0">total clicks</h6>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-center">
                                                <img src="images/chart-green.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-top padding-20">
                                        <p class="mb-0">http://invis.io/5G1YULBCE</p>
                                        <p class="mb-0 main-color" style="font-size:12px;">bit.ly/learningpagebit.ly/learning</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-shadow">
                                    <div class="padding-20">
                                        <img src="images/link-purple.svg" alt="" class="mr-2">
                                        <h6 class="text-capitalize font-weight-700 d-inline">Re-targeting Links</h6>
                                        <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-v"></i></a> </p>
                                    </div>
                                    <div class="padding-20">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <p class="mb-2 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                                <h6 class="headingtxt mb-0">total clicks</h6>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-center">
                                                <img src="images/chart-orange.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-top padding-20">
                                        <p class="mb-0">http://invis.io/5G1YULBCE</p>
                                        <p class="mb-0 main-color" style="font-size:12px;">bit.ly/learningpagebit.ly/learning</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-shadow">
                                    <div class="padding-20">
                                        <img src="images/link-red.svg" alt="" class="mr-2">
                                        <h6 class="text-capitalize font-weight-700 d-inline">Link Tree</h6>
                                        <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-v"></i></a> </p>
                                    </div>
                                    <div class="padding-20">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <p class="mb-2 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                                <h6 class="headingtxt mb-0">total clicks</h6>
                                            </div>
                                            <div class="col-lg-6 col-md-6 text-center">
                                                <img src="images/chart-blue.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-top padding-20">
                                        <p class="mb-0">http://invis.io/5G1YULBCE</p>
                                        <p class="mb-0 main-color" style="font-size:12px;">bit.ly/learningpagebit.ly/learning</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pb-5">
                    <h5 class="text-capitalize font-weight-600">Recent Short Links</h5>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col" class="wd-20">short link</th>
                                <th scope="col" class="wd-50">original link</th>
                                <th scope="col" class="wd-18">created at</th>
                                <th scope="col">total clicks</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <div class="copy"><a href="#"><i class="far fa-copy"></i></a></div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 main-color">bit.ly/learningpage</p>
                                </td>
                                <td>
                                    <p class="mb-0">https://www.behance.net/gallery/372…</p>
                                </td>
                                <td>
                                    <p class="mb-0">Jan 8, 2015, 9:48 am</p>
                                </td>
                                <td>
                                    <p class="mb-0 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="copy-edit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="copy-edit">
                                            <a class="dropdown-item" href="#">copy</a>
                                            <a class="dropdown-item" href="#">edit</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="copy"><a href="#"><i class="far fa-copy"></i></a></div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 main-color">bit.ly/learningpage</p>
                                </td>
                                <td>
                                    <p class="mb-0">https://www.behance.net/gallery/372…</p>
                                </td>
                                <td>
                                    <p class="mb-0">Jan 8, 2015, 9:48 am</p>
                                </td>
                                <td>
                                    <p class="mb-0 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="copy-edit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="copy-edit">
                                            <a class="dropdown-item" href="#">copy</a>
                                            <a class="dropdown-item" href="#">edit</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="copy"><a href="#"><i class="far fa-copy"></i></a></div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 main-color">bit.ly/learningpage</p>
                                </td>
                                <td>
                                    <p class="mb-0">https://www.behance.net/gallery/372…</p>
                                </td>
                                <td>
                                    <p class="mb-0">Jan 8, 2015, 9:48 am</p>
                                </td>
                                <td>
                                    <p class="mb-0 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="copy-edit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="copy-edit">
                                            <a class="dropdown-item" href="#">copy</a>
                                            <a class="dropdown-item" href="#">edit</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-md-12 show-more text-right mt-3">
                        <a href="#"> show more</a>
                    </div>

                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

    <script>
        var slider = $("#percentage")[0];
        var first = $("#first")[0];
        var second = $("#second")[0];
        first.innerHTML = slider.value + '%';
        second.innerHTML = slider.value + '%';
        slider.oninput = function() {
            first.innerHTML = this.value + '%';
            second.innerHTML = 100 - this.value + '%'
        }
    </script>

</body>

</html>