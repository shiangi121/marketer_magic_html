<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/setting-billing.css">
    <link rel="stylesheet" href="css/planupgrade.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>
    <style>
        .main-header form,
        .main-header form input::placeholder {
            color: #ffffff;
        }
        
        .main-header .dropdown-toggle {
            color: #ffffff;
        }
        
        #notificationdropdown svg path {
            fill: #fff;
        }
        
        .referral-menu {
            color: #fff !important;
        }
        
        #userdropdown svg path {
            stroke: #fff;
        }
    </style>
</head>

<body>
    <div class="wrapper">

        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>

        <!-- Page Content  -->
        <div id="content" class="active">

            <div id="settings" class="pb-50">
                <div class="settings-banner"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="text-center text-uppercase main-color">all upgrade popup</h1>
                        </div>
                        <div class="col-md-12">
                            <!--  CREDIT_RUN_OUT -->
                            <button type="button" class="btn linear-btn" data-toggle="modal" data-target="#creditrunoutold">
                                credit run out old
                            </button>

                            <!-- CREDIT_RUN_OUT Modal -->
                            <div class="modal fade plan-modal" id="creditrunoutold" tabindex="-1" role="dialog" aria-labelledby="creditrunoutoldTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body padding-0">
                                            <button type="button" class="close mt-2 mr-3" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <div class="padding-30 text-center">
                                                <img src="images/warning.svg" class="img-fluid mb-3" alt="">
                                                <h5 class="modal-heading">Upgrade and Save 40% Instantly</h5>
                                                <p class="modal-infotext">We Don’t Want You to Miss Out - You Could Save Up to 40% Instantly by Upgrading to the Annual Plan! Or for a Limited Time Join the Underground League of Millionaire Marketers and Unleash a Conversion Firestorm
                                                    on Your Audience by Joining the ‘Baller’ Plan!</p>
                                            </div>
                                            <div class="light-bg-box padding-20 pt-0">
                                                <p class="currentplan-name">
                                                    Your Current Plan: <span class="main-color">ClickProof Monthly $29/m</span>
                                                </p>
                                                <div class="row">
                                                    <div class="col-md-6 text-center">
                                                        <button class="btn upgradeplan">Upgrade to Annual!</button>
                                                        <p class="plan-monthly-price">
                                                            Only $19/Month! <span class="main-color">(SAVE 40%!)</span>
                                                        </p>
                                                        <p class="plan-annual-price">$228 Billed Annual</p>
                                                    </div>
                                                    <div class="col-md-6 text-center">
                                                        <button class="btn ballerbtn">Become a Baller</button>
                                                        <p class="plan-monthly-price">
                                                            $47 per month <span class="main-color">(SAVE 40%!)</span>
                                                        </p>
                                                        <p class="plan-annual-price">$228 Billed Annual</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="condition-text padding-10 text-center">
                                                <p class="mb-0">*Upgrade to Annual and Receive a Refund for Your Currently Unused Monthly Plan Instantly!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- OrderPopup  -->
                            <button type="button" class="btn linear-btn" data-toggle="modal" data-target="#OrderPopup">
                                Order Popup old
                            </button>
                            <!-- OrderPopup Modal -->
                            <div class="modal fade pricingorder-modal" id="OrderPopup" tabindex="-1" role="dialog" aria-labelledby="OrderPopupTitle" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content border-0">
                                        <div class="modal-body padding-0 border-0">
                                            <button type="button" class="close mt-2 mr-3" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <div class="text-center padding-30 pt-5">
                                                <h4 class="text-capitalize font-weight-700">Complete Your Purchase!</h4>
                                                <p class="color-grey">Join the Next Dimension of Marketers!</p>
                                            </div>
                                            <div class="padding-30 pt-0">
                                                <p class="order-summary text-left mb-3">ORDER SUMMARY</p>
                                                <div class="order-summary-box mb-3">
                                                    <div class="padding-10 border-bottom">
                                                        <p class="mb-0 text-capitalize text-left">Current Selection:
                                                            <span class="font-weight-700" id="planType">Baller (Annual)</span>
                                                            <span class="float-right" id="planPrice">$47.00/Mo</span>
                                                        </p>
                                                    </div>
                                                    <div class="padding-10">
                                                        <p class="main-color text-capitalize text-left mb-1 font-weight-700" id="priceTotal">Total: (12 Month X $47.00/M) <span class="float-right">$564/Year</span></p>
                                                    </div>
                                                </div>
                                                <p class="mb-3" style="font-size:16px;">
                                                    Next billing date: <span class="font-weight-700" id="nextBillingDate">June 10th, 2020 </span>.
                                                </p>
                                                <div class="order-summary-payment-box mb-4">
                                                    <div class="padding-10 border-bottom py-0 card">
                                                        <input autocomplete="off" id="cc" class="form-control card-number cardnum" size="20" type="text" placeholder="Card Number">
                                                        <span class="card_icon"></span>
                                                    </div>
                                                    <div class="padding-10 py-0">
                                                        <div class="form-row">
                                                            <div class="forn-group col-md-6 border-right form-row cardexpdate">
                                                                <input class="form-control col-md-3 text-right card-expiry-month" id="expdate" placeholder="MM" maxlength="2" size="2" type="text" onkeypress="javascript:return digitOnly(event)">
                                                                <span>/</span>
                                                                <input class="form-control col-md-4 text-left card-expiry-year" placeholder="YY" size="4" maxlength="2" type="text" onkeypress="javascript:return digitOnly(event)">
                                                            </div>
                                                            <div class="forn-group col-md-6">
                                                                <input autocomplete="off" class="form-control card-cvc cardcvv" placeholder="ex. 311" maxlength="4" type="text" onkeypress="javascript:return digitOnly(event)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn grediant-btn w-100" id="totalPay">Pay $564</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--  CREDIT_RUN_OUT new design -->
                            <button type="button" class="btn linear-btn" data-toggle="modal" data-target="#creditrunout">
                                credit run out new design
                            </button>

                            <!-- CREDIT_RUN_OUT Modal new design -->
                            <div class="modal fade plan-modal" id="creditrunout" tabindex="-1" role="dialog" aria-labelledby="creditrunoutTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body padding-0">
                                            <div class="padding-30 text-center">
                                                <img src="images/warning.svg" class="img-fluid mb-3" alt="">
                                                <h5 class="modal-heading px-3">Whoops! Looks like your ClickProof trial has ended</h5>
                                                <p class="modal-infotext my-4">Choose from one of our plans to continue.</p>
                                                <button class="btn ballerbtn">Upgrade Now</button>
                                            </div>
                                            <div class="padding-20 px-5 text-center light-popup-bg">
                                                <p class="mb-0">
                                                    If you have any questions, get in touch with us and we’d be happy to help you out. <a href="#" class="main-color">Get in touch.</a></p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!--  Upgrade account -->
                            <button type="button" class="btn linear-btn" data-toggle="modal" data-target="#upgradeaccont">
                                upgrade account
                            </button>

                            <!-- Upgrade account Modal -->
                            <div class="modal fade plan-modal" id="upgradeaccont" tabindex="-1" role="dialog" aria-labelledby="upgradeaccontTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered custom-maxwd-600" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body padding-0">
                                            <button type="button" class="close mt-2 mr-3" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                            <div class="padding-30 text-center">
                                                <h5 class="modal-heading mb-0">Upgrade Your Account</h5>
                                            </div>
                                            <div class="upgradepopuptabs">
                                                <ul class="nav nav-pills mb-0 justify-content-center border-bottom" id="pills-tab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="pills-baller-tab" data-toggle="pill" href="#pills-baller" role="tab" aria-controls="pills-baller" aria-selected="true">baller</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-pag-tab" data-toggle="pill" href="#pills-pag" role="tab" aria-controls="pills-pag" aria-selected="false">pay as you grow</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content padding-30" id="pills-tabContent">
                                                    <div class="tab-pane fade show active" id="pills-baller" role="tabpanel" aria-labelledby="pills-baller-tab">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <p class="text-uppercase font-weight-700">WHAT’S INCLUDED</p>
                                                                <ul class="list-styled">
                                                                    <li>10,000 Impressions Included per month</li>
                                                                    <li>5 Employees included</li>
                                                                    <li>10,000 Emails inlcuded per month</li>
                                                                    <li>100 Searches Included per month</li>
                                                                    <li>10,000 Clicks Included per month</li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div>
                                                                    <label class="radio-container mr-4">Monthly
                                                                        <input type="radio" checked="checked" name="radio1">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <label class="radio-container">Annually
                                                                        <input type="radio" name="radio1">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <p class="mt-3 mb-0 pricing-details">
                                                                        <del>$97</del>
                                                                        <span><b>$47</b></span>
                                                                        <sup class="text-capitalize">/Month</sup>
                                                                    </p>
                                                                    <p class="text-capitalize">Billed Annualy</p>
                                                                </div>
                                                                <div class="form-inline my-3">
                                                                    <p class="font-weight-600">Change your monthly visitors</p>
                                                                    <input type="range" step="10" min="0" max="100" value="0" class="pricingslider" id="pricepersentage">
                                                                    <ul class="pricerange mt-2">
                                                                        <li><input disabled type="text" name="" id="" value="10k" placeholder="10k"></li>
                                                                        <li> <input disabled type="text" name="" id="" value="20k" placeholder="20k"></li>
                                                                        <li><input disabled type="text" name="" id="" value="30k" placeholder="30k"></li>
                                                                        <li> <input disabled type="text" name="" id="" value="50k" placeholder="50k"></li>
                                                                        <li><input disabled type="text" name="" id="" value="100k+" placeholder="100k+"></li>
                                                                    </ul>
                                                                </div>
                                                                <button class="btn ballerbtn w-100 text-capitalize">purches now</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="pills-pag" role="tabpanel" aria-labelledby="pills-pag-tab">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <select class="custom-select mb-4" name="select_app" id="select_app" required="">
                                                                    <option value="">ClickProof</option>
                                                                </select>
                                                                <p class="text-uppercase font-weight-700">WHAT’S INCLUDED</p>
                                                                <ul>
                                                                    <li>Less than 10,000 impressions</li>
                                                                    <!-- if workhub is there as single app li -->
                                                                    <li>
                                                                        <div class="workhubcredit">
                                                                            <button style="vertical-align: text-bottom;" type="button" class="btn btn-number padding-0" data-type="minus" data-field="quant[1]">
                                                                                <img src="images/removeround.svg" class="img-fluid" alt="">
                                                                            </button>
                                                                            <input type="text" name="quant[1]" class="input-number" value="1" min="1" max="100" placeholder="1">
                                                                            <label for="input-number" class="mb-0 mr-1">Employee</label>
                                                                            <button style="vertical-align: text-bottom;" type="button" class="btn btn-number padding-0" data-type="plus" data-field="quant[1]">
                                                                                <img src="images/addround.svg" class="img-fluid" alt="">
                                                                            </button>
                                                                        </div>
                                                                    </li>
                                                                    <!-- if workhub is there as single app li ends  -->
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div>
                                                                    <label class="radio-container mr-4">Monthly
                                                                        <input type="radio" checked="checked" name="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <label class="radio-container">Annually
                                                                        <input type="radio" name="radio">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                    <p class="mt-3 mb-0 pricing-details">
                                                                        <del>$29</del>
                                                                        <span><b>$19</b></span>
                                                                        <sup class="text-capitalize">/Month</sup>
                                                                    </p>
                                                                    <p class="text-capitalize">Billed Annualy</p>
                                                                </div>
                                                                <button class="btn ballerbtn w-100 text-capitalize">purches now</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--  complete purchase new design -->
                            <button type="button" class="btn linear-btn" data-toggle="modal" data-target="#completepurchase">
                                complete purchase
                            </button>

                            <!-- complete purchase Modal new design -->
                            <div class="modal fade plan-modal" id="completepurchase" tabindex="-1" role="dialog" aria-labelledby="completepurchaseTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body padding-0 border-0">
                                            <button type="button" class="close mt-2 mr-3" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            <div class="text-center padding-30" style="background: #EEEEEE;border-radius: 9px 9px 0 0;">
                                                <img src="images/mmcrown.svg" class="img-fluid mmcrown-img" alt="">
                                                <h4 class="text-capitalize font-weight-700">Complete Your Purchase!</h4>
                                                <p class="color-grey mb-0">Join the Next Dimension of Marketers!</p>
                                            </div>
                                            <div class="padding-30 pt-4" style="background: #F8F8F8;border-radius: 0 0 9px 9px;">
                                                <p class="order-summary color-grey text-left mb-2">ORDER SUMMARY</p>
                                                <div class="order-summary-box mb-3">
                                                    <div class="padding-10 border-bottom">
                                                        <p class="mb-0 text-capitalize text-left">Current Selection:
                                                            <span class="font-weight-700" id="planType">Baller (Annual)</span>
                                                            <span class="float-right" id="planPrice">$47.00/Mo</span>
                                                        </p>
                                                    </div>
                                                    <div class="padding-10">
                                                        <p class="main-color text-capitalize text-left mb-1 font-weight-700" id="priceTotal">Total: (12 Month X $47.00/M) <span class="float-right">$564/Year</span></p>
                                                    </div>
                                                </div>
                                                <p class="mb-3 text-center" style="font-size:16px;">
                                                    Next billing date: <span class="font-weight-700" id="nextBillingDate">June 10th, 2020 </span>.
                                                </p>
                                                <div class="order-summary-payment-box mb-4">
                                                    <div class="padding-10 border-bottom py-0 card">
                                                        <input autocomplete="off" id="cc" class="form-control card-number cardnum" size="20" type="text" placeholder="Card Number">
                                                        <span class="card_icon"></span>
                                                    </div>
                                                    <div class="padding-10 py-0">
                                                        <div class="form-row">
                                                            <div class="forn-group col-md-6 border-right form-row cardexpdate">
                                                                <input class="form-control col-md-3 text-right card-expiry-month" id="expdate" placeholder="MM" maxlength="2" size="2" type="text" onkeypress="javascript:return digitOnly(event)">
                                                                <span>/</span>
                                                                <input class="form-control col-md-4 text-left card-expiry-year" placeholder="YY" size="4" maxlength="2" type="text" onkeypress="javascript:return digitOnly(event)">
                                                            </div>
                                                            <div class="forn-group col-md-6">
                                                                <input autocomplete="off" class="form-control card-cvc cardcvv" placeholder="ex. 311" maxlength="4" type="text" onkeypress="javascript:return digitOnly(event)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn grediant-btn w-100" id="totalPay">Pay $564</button>
                                                <div class="text-center">
                                                    <img src="images/Bitmap.svg" class="img-fluid mx-3" alt="">
                                                    <img src="images/stripe.svg" class="img-fluid mx-3" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Footer  -->
                <?php include 'footer.html';?>
            </div>

        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
</body>

</html>