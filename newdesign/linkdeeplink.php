<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/urlshortner.css">

    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="clickproof">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h4 class="font-weight-500">Deep Link</h4>
                        <p class="color-grey"> <span class="mr-2"><i class="fas fa-globe-americas"></i></span> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <button type="button" onclick="window.location.href='linkrdeeplinkcreate.php'" class="btn linear-btn linear-btn-shadow"> Create New Link</button>
                    </div>
                </div>
                <div class="row py-5">
                    <div class="col-lg-3 col-md-16 col-sm-12">
                        <div class="box-shadow">
                            <div class="padding-20">
                                <img src="images/link-purple.svg" alt="" class="mr-2">
                                <h6 class="text-capitalize font-weight-700 d-inline">deep link</h6>
                                <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-v"></i></a> </p>
                            </div>
                            <div class="padding-20">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <p class="mb-2 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                        <h6 class="headingtxt mb-0">total clicks</h6>
                                    </div>
                                    <div class="col-lg-6 col-md-6 text-center">
                                        <img src="images/chart-orange.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="border-top padding-20">
                                <p class="main-color mb-0">
                                    mini.me/2Wtv2ll
                                    <span class="float-right"><a href="#" class="copy-link"><i class="far fa-copy"></i>copy</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-16 col-sm-12">
                        <div class="box-shadow">
                            <div class="padding-20">
                                <img src="images/link-purple.svg" alt="" class="mr-2">
                                <h6 class="text-capitalize font-weight-700 d-inline">deep link</h6>
                                <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-v"></i></a> </p>
                            </div>
                            <div class="padding-20">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <p class="mb-2 font-weight-600">293 <img src="images/chartsvg.svg" alt=""> </p>
                                        <h6 class="headingtxt mb-0">total clicks</h6>
                                    </div>
                                    <div class="col-lg-6 col-md-6 text-center">
                                        <img src="images/chart-orange.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="border-top padding-20">
                                <p class="main-color mb-0">
                                    mini.me/2Wtv2ll
                                    <span class="float-right"><a href="#" class="copy-link"><i class="far fa-copy"></i>copy</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-16 col-sm-12">
                        <div class="creatcamp text-center">
                            <a href="#"><img src="images/mountains.png" class="img-fluid mb-3" alt="">
                                <p class="main-color">create new link</p>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>