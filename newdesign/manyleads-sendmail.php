<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/manyleads.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="manyleads">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="text-capitalize color-grey font-weight-200">
                            <a href="manyleads.php">
                                <svg width="15" height="15" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.57256 0.148624C4.76329 -0.0495412 5.08072 -0.0495412 5.27812 0.148624C5.46885 0.340094 5.46885 0.658764 5.27812 0.849788L1.70054 4.4413H13.5061C13.7813 4.44175 14 4.66134 14 4.93761C14 5.21388 13.7813 5.44061 13.5061 5.44061H1.70054L5.27812 9.02542C5.46885 9.22359 5.46885 9.54271 5.27812 9.73373C5.08072 9.93189 4.76285 9.93189 4.57256 9.73373L0.148047 5.29198C-0.0493488 5.10051 -0.0493488 4.78184 0.148047 4.59082L4.57256 0.148624Z" fill="#9B9B9B"></path>
                                    </svg>
                                <span class="pl-2" style="vertical-align: middle;">Dashboard</span>
                            </a>
                        </h5>
                        <h5 class="text-capitalize font-weight-600 my-3">Create Email</h5>
                    </div>
                </div>
                <div class="row py-3">
                    <form action="" class="create-form">
                        <h5 class="text-capitalize font-weight-700 mb-4 text-center">Mailing Details</h5>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="fromemail">From Email</label>
                                <input type="email" class="form-control" id="fromemail" placeholder="Email Address">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="toemail">To Email</label>
                                <input type="email" class="form-control" id="toemail" placeholder="Email Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="msg">Message</label>
                            <textarea class="form-control" id="msg" rows="3" placeholder="Type your message here…"></textarea>
                        </div>
                        <button class="btn linear-btn mt-3" type="button">send</button>
                    </form>
                </div>
                <div class="row py-5">
                    <div class="col-md-12 text-right">
                        <button class="btn export-btn"> <span class="mr-2"><i class="fas fa-file-csv"></i></span> Export CSV</button>
                    </div>
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col" class="wd-12">name</th>
                                <th scope="col" class="wd-21">website</th>
                                <th scope="col">email</th>
                                <th scope="col">phone number</th>
                                <th scope="col">date</th>
                                <th scope="col" class="wd-32">address</th>
                                <th scope="col">social media</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1"></label>
                                    </div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 text-capitalize">Adrian Smith</p>
                                </td>
                                <td>
                                    <a href="#" class="main-color mb-0">https://d7leadfinder.com</a>
                                </td>
                                <td>
                                    <p class="mb-0">adrian@gmail.com</p>
                                </td>
                                <td>
                                    <p class="mb-0">+1 920 9394 820</p>
                                </td>
                                <td>
                                    <p class="mb-0">05/02/2019</p>
                                </td>
                                <td>
                                    <p class="mb-0">345, Hickory Street, Salt Lake City</p>
                                </td>
                                <td>
                                    <a href="#" class="mr-2"><img src="images/facebook.svg" alt=""></a>
                                    <a href="#" class="mr-2"><img src="images/insta.svg" alt=""></a>
                                    <a href="#"><img src="images/twitter.svg" alt=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                    </div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 text-capitalize">Adrian Smith</p>
                                </td>
                                <td>
                                    <a href="#" class="main-color mb-0">https://d7leadfinder.com</a>
                                </td>
                                <td>
                                    <p class="mb-0">adrian@gmail.com</p>
                                </td>
                                <td>
                                    <p class="mb-0">+1 920 9394 820</p>
                                </td>
                                <td>
                                    <p class="mb-0">05/02/2019</p>
                                </td>
                                <td>
                                    <p class="mb-0">345, Hickory Street, Salt Lake City</p>
                                </td>
                                <td>
                                    <a href="#" class="mr-2"><img src="images/facebook.svg" alt=""></a>
                                    <a href="#" class="mr-2"><img src="images/insta.svg" alt=""></a>
                                    <a href="#"><img src="images/twitter.svg" alt=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label" for="customCheck3"></label>
                                    </div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 text-capitalize">Adrian Smith</p>
                                </td>
                                <td>
                                    <a href="#" class="main-color mb-0">https://d7leadfinder.com</a>
                                </td>
                                <td>
                                    <p class="mb-0">adrian@gmail.com</p>
                                </td>
                                <td>
                                    <p class="mb-0">+1 920 9394 820</p>
                                </td>
                                <td>
                                    <p class="mb-0">05/02/2019</p>
                                </td>
                                <td>
                                    <p class="mb-0">345, Hickory Street, Salt Lake City</p>
                                </td>
                                <td>
                                    <a href="#" class="mr-2"><img src="images/facebook.svg" alt=""></a>
                                    <a href="#" class="mr-2"><img src="images/insta.svg" alt=""></a>
                                    <a href="#"><img src="images/twitter.svg" alt=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                                        <label class="custom-control-label" for="customCheck4"></label>
                                    </div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 text-capitalize">Adrian Smith</p>
                                </td>
                                <td>
                                    <a href="#" class="main-color mb-0">https://d7leadfinder.com</a>
                                </td>
                                <td>
                                    <p class="mb-0">adrian@gmail.com</p>
                                </td>
                                <td>
                                    <p class="mb-0">+1 920 9394 820</p>
                                </td>
                                <td>
                                    <p class="mb-0">05/02/2019</p>
                                </td>
                                <td>
                                    <p class="mb-0">345, Hickory Street, Salt Lake City</p>
                                </td>
                                <td>
                                    <a href="#" class="mr-2"><img src="images/facebook.svg" alt=""></a>
                                    <a href="#" class="mr-2"><img src="images/insta.svg" alt=""></a>
                                    <a href="#"><img src="images/twitter.svg" alt=""></a>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck5">
                                        <label class="custom-control-label" for="customCheck5"></label>
                                    </div>
                                </td>
                                <td scope="row">
                                    <p class="mb-0 text-capitalize">Adrian Smith</p>
                                </td>
                                <td>
                                    <a href="#" class="main-color mb-0">https://d7leadfinder.com</a>
                                </td>
                                <td>
                                    <p class="mb-0">adrian@gmail.com</p>
                                </td>
                                <td>
                                    <p class="mb-0">+1 920 9394 820</p>
                                </td>
                                <td>
                                    <p class="mb-0">05/02/2019</p>
                                </td>
                                <td>
                                    <p class="mb-0">345, Hickory Street, Salt Lake City</p>
                                </td>
                                <td>
                                    <a href="#" class="mr-2"><img src="images/facebook.svg" alt=""></a>
                                    <a href="#" class="mr-2"><img src="images/insta.svg" alt=""></a>
                                    <a href="#"><img src="images/twitter.svg" alt=""></a>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <div class="col-md-12 show-more text-right mt-3">
                        <a href="#"> show more</a>
                    </div>
                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>