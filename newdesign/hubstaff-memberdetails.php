<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaffmemberdetails">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <a href="hubstaff-member.php" class="color-grey text-capitalize"><span class="mr-2"><svg width="15" height="15" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4.57256 0.148624C4.76329 -0.0495412 5.08072 -0.0495412 5.27812 0.148624C5.46885 0.340094 5.46885 0.658764 5.27812 0.849788L1.70054 4.4413H13.5061C13.7813 4.44175 14 4.66134 14 4.93761C14 5.21388 13.7813 5.44061 13.5061 5.44061H1.70054L5.27812 9.02542C5.46885 9.22359 5.46885 9.54271 5.27812 9.73373C5.08072 9.93189 4.76285 9.93189 4.57256 9.73373L0.148047 5.29198C-0.0493488 5.10051 -0.0493488 4.78184 0.148047 4.59082L4.57256 0.148624Z" fill="#9B9B9B"></path>
                        </svg></span> members</a>
                        <h5 class="text-capitalize font-weight-600 mt-4">Member Details</h5>
                        <p class="color-grey"> <i class="fas fa-question-circle"></i> Time totals may not match reports. <a href="#" class="main-color">Why?</a></p>
                    </div>

                </div>
                <div class="row pt-3 filters">
                    <div class="col-md-12 col-lg-5">
                        <form action="" class="">
                            <div class="form-group">
                                <label for="">member</label>
                                <select class="custom-select">
                                    <option selected>Adrian Smith</option>
                                    <option value="1">John Deo</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">source</label>
                                <select class="custom-select">
                                    <option selected>All Types</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">timezone</label>
                                <select class="custom-select">
                                    <option selected>My time zone (Asia)</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12 col-lg-7">
                        <label for="" class="d-block">filter</label>
                        <a href="#"><i class="fas fa-arrow-left"></i></a>
                        <a href="#"><i class="fas fa-arrow-right"></i></a>
                        <div class="form-group">
                            <input type="date" class="form-control" id="">
                        </div>
                        <a href="#">today</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                        <p class="color-grey my-3"><b class="color-black text-capitalize">Today</b> 2:09:29 hrs</p>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn cancel-btn mr-4" type="button">add time</button>
                        <button class="btn linear-btn linear-btn-shadow" type="button">export</button>
                    </div>
                </div>
                <div class="row py-5">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1"></label>
                                    </div>
                                </th>
                                <th scope="col" class="wd-32">project</th>
                                <th scope="col">manual</th>
                                <th scope="col" class="wd-32">duration</th>
                                <th scope="col" class="wd-32">time span</th>
                                <th scope="col" class="wd-32"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="project-info">
                                        <div class="float-left mr-3">
                                            <div class="project-initial bg-lightred">
                                                <p class="text-capitalize mb-0">h</p>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="project-name">
                                                <a href="#" class="font-weight-600 mb-0 d-inline">MarketerMagic Design Team</a>
                                                <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                    <i class="fas fa-question-circle"></i>
                                                </button>
                                            </div>
                                            <div class="project-head">
                                                <p class="mb-0">Thatslifestyleninja</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0">0%</p>
                                </td>
                                <td>
                                    <p class="mb-0">2:09:22</p>
                                </td>
                                <td>
                                    <p class="mb-0">7:01:36 am - 7:01:43 am</p>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">Edit time entry</a>
                                            <a class="dropdown-item" href="#">Split time entry</a>
                                            <a class="dropdown-item" href="#">Delete time entry</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label" for="customCheck3"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="project-info">
                                        <div class="float-left mr-3">
                                            <div class="project-initial bg-lightblue">
                                                <p class="text-capitalize mb-0">m</p>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="project-name">
                                                <a href="#" class="font-weight-600 mb-0 d-inline">MarketerMagic Dev Team</a>
                                                <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                    <i class="fas fa-question-circle"></i>
                                                </button>
                                            </div>
                                            <div class="project-head">
                                                <p class="mb-0">Thatslifestyleninja</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0">0%</p>
                                </td>
                                <td>
                                    <p class="mb-0">2:09:22</p>
                                </td>
                                <td>
                                    <p class="mb-0">7:01:36 am - 7:01:43 am</p>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">Edit time entry</a>
                                            <a class="dropdown-item" href="#">Split time entry</a>
                                            <a class="dropdown-item" href="#">Delete time entry</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>

</html>