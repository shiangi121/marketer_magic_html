<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link href="css/lightgallery.css" rel="stylesheet">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaff-activity">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-capitalize font-weight-600">screenshot activity</h3>
                        <p class="color-grey"><i class="fas fa-globe-asia"></i> Time totals on this screen will not match reports if you are in a different timezone than the organization. <a href="#" class="main-color">View reports</a> for this day.</p>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-lg-4 col-md-6">
                        <div class="box-shadow activity-box">
                            <div class="padding-20 pb-0">
                                <div class="title d-inline">
                                    <div class="bg-yellow padding-10 mr-1">
                                        <img src="images/activity.svg" class="img-fluid" alt="">
                                    </div>
                                    <h5 class="text-capitalize font-weight-600 d-inline">activity</h5>
                                </div>
                                <div class="float-right color-grey"><i class="fas fa-ellipsis-v"></i></div>
                            </div>
                            <div class="padding-20">
                                <div class="number">
                                    <p class="mb-0 d-inline">75%</p>
                                    <img src="images/graph.png" class="img-fluid float-right" alt="">
                                </div>
                                <div class="percount">
                                    <div class="positive-per">
                                        <p class="mb-0"><i class="fas fa-caret-up"></i> 10%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="box-shadow activity-box">
                            <div class="padding-20 pb-0">
                                <div class="title d-inline">
                                    <div class="bg-yellow padding-10 mr-1">
                                        <img src="images/activity.svg" class="img-fluid" alt="">
                                    </div>
                                    <h5 class="text-capitalize font-weight-600 d-inline">Worked this week</h5>
                                </div>
                                <div class="float-right color-grey"><i class="fas fa-ellipsis-v"></i></div>
                            </div>
                            <div class="padding-20">
                                <div class="number">
                                    <p class="mb-0 d-inline">26:53:12</p>
                                    <img src="images/map1.png" class="img-fluid float-right" alt="">
                                </div>
                                <div class="percount">
                                    <div class="negative-per">
                                        <p class="mb-0"><i class="fas fa-caret-down"></i> 21:37:08</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="box-shadow activity-box">
                            <div class="padding-20 pb-0">
                                <div class="title d-inline">
                                    <div class="bg-yellow padding-10 mr-1">
                                        <img src="images/activity.svg" class="img-fluid" alt="">
                                    </div>
                                    <h5 class="text-capitalize font-weight-600 d-inline">Earned this week</h5>
                                </div>
                                <div class="float-right color-grey"><i class="fas fa-ellipsis-v"></i></div>
                            </div>
                            <div class="padding-20">
                                <div class="number">
                                    <p class="mb-0 d-inline">$359.72</p>
                                    <img src="images/graph.png" class="img-fluid float-right" alt="">
                                </div>
                                <div class="percount">
                                    <div class="negative-per">
                                        <p class="mb-0"><i class="fas fa-caret-down"></i> $36.20</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-5 filters">
                    <div class="col-md-12 col-lg-5">
                        <form action="" class="">
                            <div class="form-group">
                                <label for="">member</label>
                                <select class="custom-select">
                                    <option selected>Adrian Smith</option>
                                    <option value="1">John Deo</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">project</label>
                                <select class="custom-select">
                                    <option selected>All Projects</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">timezone</label>
                                <select class="custom-select">
                                    <option selected>My time zone (Asia)</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12 col-lg-7">
                        <label for="" class="d-block">filter</label>
                        <a href="#"><i class="fas fa-arrow-left"></i></a>
                        <a href="#"><i class="fas fa-arrow-right"></i></a>
                        <div class="form-group">
                            <input type="date" class="form-control" id="">
                        </div>
                        <a href="#">today</a>
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="col-md-12">
                        <!-- The Timeline -->
                        <ul class="timeline">
                            <li>
                                <div class="direction-l">
                                    <div class="flag-wrapper">
                                        <p class="time-intervals"> <b>7:00 am - 8:00 am</b> total time worked: <b>0:49:29</b></p>
                                        <div class="flag gallery padding-20">
                                            <ul id="lightgallery" class="list-unstyled row">
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="positive-activity">
                                                            <p class="mb-0">75%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar positive-progress" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>80% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="average-activity">
                                                            <p class="mb-0">45%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar average-progress" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>45% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="negative-activity">
                                                            <p class="mb-0">19%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar negative-progress" role="progressbar" style="width: 19%;" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>19% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="positive-activity">
                                                            <p class="mb-0">75%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar positive-progress" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>80% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="average-activity">
                                                            <p class="mb-0">45%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar average-progress" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>45% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="negative-activity">
                                                            <p class="mb-0">19%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar negative-progress" role="progressbar" style="width: 19%;" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>19% of 7 seconds</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="direction-l">
                                    <div class="flag-wrapper">
                                        <p class="time-intervals"> <b>7:00 am - 8:00 am</b> total time worked: <b>0:49:29</b></p>
                                        <div class="flag gallery padding-20">
                                            <ul id="lightgallery1" class="list-unstyled row">
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="positive-activity">
                                                            <p class="mb-0">75%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar positive-progress" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>80% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="average-activity">
                                                            <p class="mb-0">45%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar average-progress" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>45% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="negative-activity">
                                                            <p class="mb-0">19%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar negative-progress" role="progressbar" style="width: 19%;" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>19% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="positive-activity">
                                                            <p class="mb-0">75%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar positive-progress" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>80% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="average-activity">
                                                            <p class="mb-0">45%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar average-progress" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>45% of 7 seconds</p>
                                                    </div>
                                                </li>
                                                <li class="col-xs-12 col-sm-12 col-md-4 col-lg-2" data-src="images/lightbox/activity-img.png">
                                                    <div class="activityper">
                                                        <div class="negative-activity">
                                                            <p class="mb-0">19%</p>
                                                        </div>
                                                    </div>
                                                    <a href="" class="on-hover">
                                                        <img class="img-responsive" src="images/lightbox/activity-img.png">
                                                        <div class="hover-box">
                                                            <p class="view-screen">view screen</p>
                                                        </div>
                                                    </a>
                                                    <div class="activity-info padding-20">
                                                        <p class="timespan">7:00 am - 7:10 am </p>
                                                        <div class="progress">
                                                            <div class="progress-bar negative-progress" role="progressbar" style="width: 19%;" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                        <p>19% of 7 seconds</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#lightgallery').lightGallery();
            $('#lightgallery1').lightGallery();
        });
    </script>
    <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
    <script src="js/lightgallery-all.min.js"></script>
    <script src="js/jquery.mousewheel.min.js"></script>
</body>

</html>