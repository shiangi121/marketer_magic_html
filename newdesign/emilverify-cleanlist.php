<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/emilverify.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5  mt-77" id="emilverifylistview">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h5 class="font-weight-600 text-capitalize">clean new list</h5>
                    </div>
                </div>
                <div class="row cleanalist-header pt-4">
                    <div class="col-md-3">
                        <p class="heading">email verification</p>
                        <p class="text">March 01, 2019 — March 31, 2019</p>
                    </div>
                    <div class="col-md-3">
                        <p class="heading">records Verified</p>
                        <p class="text">30,931</p>
                    </div>
                    <div class="col-md-3">
                        <p class="heading">total cost</p>
                        <a href="#" class="main-color">See invoice</a>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group date">
                            <input type="date" class="form-control" id="">
                        </div>
                    </div>
                </div>
                <div class="row cleanalist my-5">
                    <div class="col-lg-4 col-md-12">
                        <div class="box-shadow total-emails">
                            <div class="text-center padding-20">
                                <h5 class="text-capitalize font-weight-600">total emails</h5>
                                <div class="chart-box">
                                    <canvas id="totalemails-chart" class="my-3"></canvas>
                                    <p class="lower-text text-uppercase">total</p>
                                </div>
                            </div>
                            <div class="row totalemailscount px-5">
                                <div class="col-md-6">
                                    <p class="valid text-capitalize">
                                        <span>18395</span>92%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="acceptall text-capitalize">
                                        <span>4748</span>10%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="unknown text-capitalize">
                                        <span>4564</span>50%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="invalid text-capitalize">
                                        <span>8798</span>60%
                                    </p>
                                </div>
                            </div>
                            <div class="padding-20 text-center border-top">
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 valid"><span class="mr-2"><i class="fas fa-circle"></i></span>valid</p>
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 acceptall"><span class="mr-2"><i class="fas fa-circle"></i></span>accept all</p>
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 unknown"><span class="mr-2"><i class="fas fa-circle"></i></span>unknown</p>
                                <p class="mb-0 d-inline text-capitalize color-grey invalid"><span class="mr-2"><i class="fas fa-circle"></i></span>invalid</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="box-shadow invalid-emails">
                            <div class="text-center padding-20">
                                <h5 class="text-capitalize font-weight-600">invalid emails</h5>
                                <div class="chart-box">
                                    <canvas id="invalidemails-chart" class="my-3"></canvas>
                                    <p class="lower-text text-uppercase">total</p>
                                </div>
                            </div>
                            <div class="row invalidemailscount px-5">
                                <div class="col-md-6">
                                    <p class="accountinvalid text-capitalize">
                                        <span>3</span>75%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="domaininvalid text-capitalize">
                                        <span>1</span>25%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="adressinvalid text-capitalize">
                                        <span>0</span>0%
                                    </p>
                                </div>

                            </div>
                            <div class="padding-20 text-center border-top">
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 accountinvalid"><span class="mr-2"><i class="fas fa-circle"></i></span> account invalid</p>
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 domaininvalid"><span class="mr-2"><i class="fas fa-circle"></i></span> domain invalid</p>
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 adressinvalid"><span class="mr-2"><i class="fas fa-circle"></i></span>adress invalid</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="box-shadow risky-emails">
                            <div class="text-center padding-20">
                                <h5 class="text-capitalize font-weight-600">risky emails</h5>
                                <div class="chart-box">
                                    <canvas id="riskyemails-chart" class="my-3"></canvas>
                                    <p class="lower-text text-uppercase">total</p>
                                </div>
                            </div>
                            <div class="row riskyemailscount px-5">
                                <div class="col-md-6">
                                    <p class="acceptall text-capitalize">
                                        <span>0</span>0%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="unknown text-capitalize">
                                        <span>45,748</span>0%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="roleaddress text-capitalize">
                                        <span>444</span>51%
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="disposable text-capitalize">
                                        <span>427</span>49%
                                    </p>
                                </div>
                            </div>
                            <div class="padding-20 text-center border-top">
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 acceptall"><span class="mr-2"><i class="fas fa-circle"></i></span>accept all</p>
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 unknown"><span class="mr-2"><i class="fas fa-circle"></i></span>unknown</p>
                                <p class="mb-0 d-inline text-capitalize color-grey mr-3 roleaddress"><span class="mr-2"><i class="fas fa-circle"></i></span>role address</p>
                                <p class="mb-0 d-inline text-capitalize color-grey disposable"><span class="mr-2"><i class="fas fa-circle"></i></span>disposable</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/Chart.js"></script>
    <script>
        Chart.pluginService.register({
            beforeDraw: function(chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Lato';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#453937';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                        //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        400,
                        300,
                        800,
                        900,
                    ],

                    backgroundColor: [
                        "#02CED1",
                        "#FE7682",
                        "#7DD024",
                        "#F6EB53"
                    ],
                }],
                labels: [
                    'Valid',
                    'Accept All',
                    'Unknown',
                    'Invalid',
                ]
            },
            options: {
                responsive: true,
                legend: false,
                cutoutPercentage: 85,


                elements: {
                    center: {
                        text: '30,931',
                        color: '#453937 ', // Default is #453937
                        fontStyle: 'Lato', // Default is Lato
                        sidePadding: 50 // Defualt is 20 (as a percentage)
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
            }
        };
        var ctx = document.getElementById("totalemails-chart").getContext("2d");
        var myChart = new Chart(ctx, config);
        myChart.data.datasets[0].data[2] = 800;
        myChart.update();
    </script>
    <script>
        Chart.pluginService.register({
            beforeDraw: function(chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Lato';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#453937';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                        //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        400,
                        300,
                        800
                    ],

                    backgroundColor: [
                        "#FE7682",
                        "#CB4A4A",
                        "#FFC9CE"
                    ],
                }],
                labels: [
                    'Account Invalid',
                    'Domain Invalid',
                    'Address Invalid',
                ]
            },
            options: {
                responsive: true,
                legend: false,
                cutoutPercentage: 85,


                elements: {
                    center: {
                        text: '1,084',
                        color: '#453937', // Default is #453937
                        fontStyle: 'Lato', // Default is Lato
                        sidePadding: 50 // Defualt is 20 (as a percentage)
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
            }
        };
        var ctx = document.getElementById("invalidemails-chart").getContext("2d");
        var myChart = new Chart(ctx, config);
        myChart.data.datasets[0].data[1] = 800;
        myChart.update();
    </script>
    <script>
        Chart.pluginService.register({
            beforeDraw: function(chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;
                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Lato';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#453937';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                        //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        400,
                        300,
                        800,
                        900,
                    ],

                    backgroundColor: [
                        "#F8D01C",
                        "#FFE15A",
                        "#FFEC97",
                        "#F8D01C"
                    ],
                }],
                labels: [
                    'Accept all',
                    'Unknown',
                    'Role Adress',
                    'Disposable',
                ]
            },
            options: {
                responsive: true,
                legend: false,
                cutoutPercentage: 85,


                elements: {
                    center: {
                        text: '50,931',
                        color: '#453937', // Default is #453937
                        fontStyle: 'Lato', // Default is Lato
                        sidePadding: 50 // Defualt is 20 (as a percentage)
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
            }
        };
        var ctx = document.getElementById("riskyemails-chart").getContext("2d");
        var myChart = new Chart(ctx, config);
        myChart.data.datasets[0].data[1] = 800;
        myChart.update();
    </script>
</body>

</html>