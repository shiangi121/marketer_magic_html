<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/clickproof.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="clickproof">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h4 class="total-campaigns font-weight-500">0 of 0 Campaigns</h4>
                        <div class="all-campaign d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm">
                                <option>campaign</option>
                                <option selected>all campaigns</option>
                                <option>campaign2</option>
                            </select>
                        </div>
                        <div class="modified-date d-inline pl-5">
                            <span class="color-grey">Short by</span>
                            <select class="form-control form-control-sm">
                                <option>recent</option>
                                <option selected>Modified date</option>
                                <option>week ago</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <button class="btn linear-btn linear-btn-shadow" type="button" data-toggle="modal" data-target="#createcampaignmodal"><img src="images/announce.svg" class="img-fluid pr-3" alt="">Create Campaign</button>
                    </div>
                </div>
                <div class="text-center py-5 create-campaign">
                    <img src="images/laptop.png" class="img-fluid" alt="">
                    <div class="d-inline-block">
                        <h5 class="text-left font-weight-700 mb-3">Create your first campaign</h5>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="text" placeholder="Campaign name">
                            <button class="btn linear-btn my-2 my-sm-0" onclick="window.location.href='clickprooflist.php'" type="button">next</button>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>



    </div>
    <!-- Modal -->
    <div class="modal fade" id="createcampaignmodal" tabindex="-1" role="dialog" aria-labelledby="createcampaignmodalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body border-0 text-center">
                    <h5 class="font-weight-700 mb-3">Build & launch a new campaign</h5>
                    <form class="form-inline row pt-4 px-3">
                        <div class="col-md-9">
                            <input class="form-control w-100" type="text" placeholder="Campaign name">
                        </div>
                        <div class="col-md-3 pl-0">
                            <button class="btn linear-btn w-100" onclick="window.location.href='clickproofwizard.php'" type="button">next</button>
                        </div>
                    </form>
                    <img src="images/modal-img.png" class="img-fluid my-5" alt="">
                </div>


            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>