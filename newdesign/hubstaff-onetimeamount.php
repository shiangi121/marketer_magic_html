<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaffpayment">
                <div class="row">
                    <div class="col-md-12 mobile-center">
                        <h5 class="font-weight-600">Send Payments</h5>
                        <div class="showselection d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm" onchange="location = this.value;">
                                <option  value="hubstaff-sendpayment.php">Pay For Hours </option>
                                <option  value="hubstaff-timesheet.php">Approved Timesheets</option>
                                <option selected value="hubstaff-onetimeamount.php">One-time Amount</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row py-5">
                    <div class="onetime-payment">
                        <form action="">
                            <div class="form-group">
                                <label class="w-100" for="member">member<a href="#" class="float-right main-color">select all</a></label>
                                <select class="custom-select" id="member" aria-describedby="memberhelp">
                                    <option selected>Select member</option>
                                    <option value="1">ABC</option>
                                    <option value="2">XYZ</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="amount" class="w-100">amount per member</label>
                                <input type="number" class="form-control d-inline" style="width:50%;" name="" id="amount" placeholder="0">
                                <p class="color-grey d-inline ml-3">USD</p>
                            </div>
                            <div class="form-group">
                                <label for="note">note</label>
                                <textarea class="form-control" id="validationTextarea" placeholder="Enter a note about the payment"></textarea>
                            </div>
                            <button type="button" class="btn linear-btn linear-btn-shadow">apply</button>
                        </form>
                    </div>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>