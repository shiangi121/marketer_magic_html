<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/manyleads.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="manyleads">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h4 class="font-weight-500">ManyLeads</h4>
                        <div class="showselection d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm">
                                <option>campaign</option>
                                <option selected>Active Campaigns</option>
                                <option>campaign2</option>
                            </select>
                        </div>
                        <div class="showselection d-inline pl-5">
                            <span class="color-grey">Shorted by</span>
                            <select class="form-control form-control-sm">
                                <option>recent</option>
                                <option selected>Created at</option>
                                <option>week ago</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <button class="btn linear-btn linear-btn-shadow" onclick="window.location.href='manyleads-createcamp.php'" type="button"><img src="images/announce.svg" class="img-fluid pr-3" alt="">Create Campaign</button>
                    </div>
                </div>
                <div class="manyleads-list row py-5">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="box-shadow">
                            <div class="camp-info padding-20">
                                <h5 class="text-capitalize font-weight-700 d-inline">campaign sample 1</h5>
                                <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-h"></i></a> </p>
                                <p class="color-grey mb-0">Created 4 days ago</p>
                            </div>
                            <div class="row padding-20">
                                <div class="col-md-6">
                                    <img src="images/attorneys.svg" alt="" class="float-left mr-2">
                                    <h6 class="text-capitalize font-weight-700 mb-0" style="line-height:35px;">Medical Spas</h6>
                                </div>
                                <div class="col-md-6">
                                    <img src="images/location.svg" alt="" class="float-left mr-2">
                                    <h6 class="text-capitalize font-weight-700 mb-0">San Diego</h6>
                                    <p class="color-grey text-capitalize" style="font-size:12px;">California</p>
                                </div>
                            </div>
                            <div class="border-top padding-20">
                                <p class="d-inline color-grey">13 minutes ago</p>
                                <a href="#" class="float-right text-capitalize export mobile-float-none"> <span class="mr-1"><i class="fas fa-file-export"></i></span> export</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="box-shadow">
                            <div class="camp-info padding-20">
                                <h5 class="text-capitalize font-weight-700 d-inline">campaign sample 1</h5>
                                <p class="mb-0 float-right"> <a href="#" class="color-grey"><i class="fas fa-ellipsis-h"></i></a> </p>
                                <p class="color-grey mb-0">Created 4 days ago</p>
                            </div>
                            <div class="row padding-20">
                                <div class="col-md-6">
                                    <img src="images/attorneys.svg" alt="" class="float-left mr-2">
                                    <h6 class="text-capitalize font-weight-700 mb-0" style="line-height:35px;">Medical Spas</h6>
                                </div>
                                <div class="col-md-6">
                                    <img src="images/location.svg" alt="" class="float-left mr-2">
                                    <h6 class="text-capitalize font-weight-700 mb-0">San Diego</h6>
                                    <p class="color-grey text-capitalize" style="font-size:12px;">California</p>
                                </div>
                            </div>
                            <div class="border-top padding-20">
                                <p class="d-inline color-grey">13 minutes ago</p>
                                <a href="#" class="float-right text-capitalize export mobile-float-none"> <span class="mr-1"><i class="fas fa-file-export"></i></span> export</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="creatcamp text-center">
                            <a href="#"><img src="images/mountains.png" class="img-fluid mb-3" alt="">
                                <p class="main-color">create new link</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>