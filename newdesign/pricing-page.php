<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/setting-billing.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>
    <style>
        .main-header form,
        .main-header form input::placeholder {
            color: #ffffff;
        }
        
        .main-header .dropdown-toggle {
            color: #ffffff;
        }
        
        #notificationdropdown svg path {
            fill: #fff;
        }
        
        .referral-menu {
            color: #fff !important;
        }
        
        #userdropdown svg path {
            stroke: #fff;
        }
    </style>
</head>

<body>
    <div class="wrapper">

        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>

        <!-- Page Content  -->
        <div id="content" class="active">

            <div id="pricing">
                <div class="pricing-banner text-center">
                    <h3 class="font-weight-700">Pricing & Plans</h3>
                    <p>Beautiful conversion optimization, organization, and team management for your business</p>
                </div>
                <div class="container-fluid pricing-table">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive px-4">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">
                                            </th>
                                            <th scope="col">
                                                <div class="text-center plan-img"> <img src="images/free-plan.png" class="img-fluid" alt=""></div>
                                            </th>
                                            <th scope="col">
                                                <div class="text-center plan-img"> <img src="images/baller-plan.png" class="img-fluid" alt=""></div>
                                            </th>
                                            <th scope="col">
                                                <div class="text-center plan-img"> <img src="images/startup-plan.png" class="img-fluid" alt=""></div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="custom-bg">
                                        <tr>
                                            <td>
                                                <div style="position:relative;">
                                                    <p class="mb-0">Save $600 Instantly <br>by paying annually!
                                                        <img src="images/pricing-arrow.svg" class="img-fluid side-arrow" alt="">
                                                    </p>
                                                    <div class="select-pricing">
                                                        <a href="pricing-page-monthly.php">monthly</a>
                                                        <a href="pricing-page.php" class="active-pricing">annual</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="free-plan text-center pt-4">
                                                    <h5>free plan</h5>
                                                    <p class="mb-0"><sup>$</sup><span>0</span></p>
                                                    <p>Perfect for beginners</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="baller-plan text-center pt-4">
                                                    <h5>baller<img src="images/most-popular.png" class="img-fluid popular-img" alt=""></h5>
                                                    <p class="mb-0"><sup>$</sup><span>47</span><sup class="text-capitalize">/Mo</sup></p>
                                                    <p>Perfect for Businesses Over 50k Annual Revenue</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="startup-plan text-center pt-4">
                                                    <h5>startup</h5>
                                                    <p class="text-capitalize mb-0">pay as you grow!</p>
                                                    <p>Perfect for Businesses Under $50k Annual Revenue</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="inner-checkbox">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" checked class="custom-control-input" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">ClickProof</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style"><span class="main-color font-weight-700">500</span> Uses</p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style">
                                                    <img src="images/verified-main-color.svg" class="img-fluid" alt="">
                                                </p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style">
                                                    <p class="hover-text">
                                                        <a href="#"> <img src="images/carticon.png" class="cartimg" alt=""></a> $19 /m
                                                        <button type="button" class="btn tooltipbtn padding-0 border-0" data-toggle="tooltip" data-placement="right" title="info">
                                                            <span class="infoicon"><i class="fas fa-info-circle"></i></span>
                                                        </button>
                                                    </p>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="inner-checkbox">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" checked class="custom-control-input" id="customCheck2">
                                                        <label class="custom-control-label" for="customCheck2">ClickProof</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style"><span class="main-color font-weight-700">500</span> Uses</p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style">
                                                    <img src="images/verified-main-color.svg" class="img-fluid" alt="">
                                                </p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style"><span class="main-color font-weight-700">1$</span> per Impression</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="inner-checkbox">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" checked class="custom-control-input" id="customCheck3">
                                                        <label class="custom-control-label" for="customCheck3">ClickProof</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style"><span class="main-color font-weight-700">500</span> Uses</p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style">
                                                    <img src="images/verified-main-color.svg" class="img-fluid" alt="">
                                                </p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style">Free</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <p class="mb-0 custom-style"> <button class="btn linear-btn linear-btn-shadow my-3" type="button">choose plan</button></p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style"> <button class="btn linear-btn linear-btn-shadow my-3" type="button">choose plan</button></p>
                                            </td>
                                            <td>
                                                <p class="mb-0 custom-style"> <button class="btn linear-btn linear-btn-shadow my-3" type="button">choose plan</button></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>