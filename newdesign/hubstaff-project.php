<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaffprojects">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h5 class="font-weight-600">Projects (13)</h5>
                        <div class="showselection d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm">
                                <option selected>Active</option>
                                <option>Archived</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <form class="form-inline d-inline search-project">
                            <i class="fas fa-search"></i>
                            <input class="form-control" type="search" placeholder="Search Project" aria-label="Search">
                        </form>
                        <button class="btn linear-btn linear-btn-shadow" data-toggle="modal" data-target="#addprojectModal" type="button">Add projects</button>
                    </div>
                </div>
                <div class="row py-5">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1"></label>
                                    </div>
                                </th>
                                <th scope="col" class="wd-25">name</th>
                                <th scope="col">DESCRIPTION</th>
                                <th scope="col" class="wd-12">MEMBERS</th>
                                <th scope="col">BUDGETS</th>
                                <th scope="col">TRACKABLE BY ME</th>
                                <th scope="col">STATUS</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="project-info">
                                        <div class="float-left mr-3">
                                            <div class="project-initial bg-lightblue">
                                                <p class="text-capitalize mb-0">m</p>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="project-name">
                                                <a href="hubstaff-individualproject.php" class="font-weight-600 mb-0 d-inline">MarketerMagic Dev Team</a>
                                                <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                    <i class="fas fa-question-circle"></i>
                                                </button>
                                            </div>
                                            <div class="project-head">
                                                <p class="mb-0">Thatslifestyleninja</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et cum ratione, Et cum</p>
                                </td>
                                <td>
                                    <div class="member-initlist">
                                        <p class="memberinit bg-lightblue">
                                            a
                                        </p>
                                        <p class="memberinit bg-lightred">
                                            s
                                        </p>
                                        <img src="images/userimg.png" class="img-fluid" alt="">
                                        <p class="memberinit bg-maincolor">
                                            <i class="fas fa-plus"></i>
                                        </p>
                                        <p class="mb-0 color-grey d-inline">+ 2</p>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline">Budget: <span class="color-grey">none</span></p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="main-color mb-0 text-center"><i class="fas fa-check-circle"></i></p>
                                </td>
                                <td>
                                    <p class="mb-0 status-active">active</p>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" data-toggle="modal" data-target="#editprojectModal" href="#">edit project</a>
                                            <a class="dropdown-item" href="#">manage budget</a>
                                            <a class="dropdown-item" href="#">archive project</a>
                                            <a class="dropdown-item" data-toggle="modal" data-target="#transferprojectModal" href="#">transfer</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label" for="customCheck3"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="project-info">
                                        <div class="float-left mr-3">
                                            <div class="project-initial bg-lightred">
                                                <p class="text-capitalize mb-0">h</p>
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="project-name">
                                                <a href="#" class="font-weight-600 mb-0 d-inline">MarketerMagic Design Team</a>
                                                <button type="button" class="btn tooltipbtn" data-toggle="tooltip" data-placement="right" title="info">
                                                    <i class="fas fa-question-circle"></i>
                                                </button>
                                            </div>
                                            <div class="project-head">
                                                <p class="mb-0">Thatslifestyleninja</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et cum ratione, Et cum</p>
                                </td>
                                <td>
                                    <div class="member-initlist">
                                        <p class="memberinit bg-lightblue">
                                            k
                                        </p>

                                        <img src="images/userimg.png" class="img-fluid" alt="">
                                        <p class="memberinit bg-lightred">
                                            p
                                        </p>
                                        <p class="memberinit bg-maincolor">
                                            <i class="fas fa-plus"></i>
                                        </p>
                                        <p class="mb-0 color-grey d-inline">+ 13</p>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0 d-inline">Budget: <span class="color-grey">none</span></p>
                                    <button class="editme-button" type="button"><i class="fas fa-pen"></i></button>
                                </td>
                                <td>
                                    <p class="main-color mb-0 text-center"><i class="fas fa-check-circle"></i></p>
                                </td>
                                <td>
                                    <p class="mb-0 status-paused">paused</p>
                                </td>
                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i> 
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">edit project</a>
                                            <a class="dropdown-item" href="#">manage budget</a>
                                            <a class="dropdown-item" href="#">archive project</a>
                                            <a class="dropdown-item" href="#">transfer</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>

    <!-- Add Project Modal -->
    <div class="modal fade" id="addprojectModal" tabindex="-1" role="dialog" aria-labelledby="addprojectModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header padding-20">
                    <div class="float-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="addprojectModalTitle">New Project</h5>
                    <p class="d-block mb-0">Manage this project with WorkHub, learn more.</p>
                </div>
                <div class="modal-body padding-20">
                    <form action="">
                        <div class="form-group">
                            <label for="name">Organization</label>
                            <p class="orgo-name color-grey">THATLifestyleNinja</p>
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input required type="text" class="form-control" id="name" aria-describedby="namehelp" placeholder="Enter project names separated by new lines">
                        </div>
                        <div class="form-group">
                            <label for="client">client</label>
                            <select class="custom-select">
                                <option selected>Select Client</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <p class="main-color heading-text">team</p>
                            <p class="color-grey">Select team members to add to this project</p>
                        </div>
                        <div class="form-group">
                            <label for="manager">manager</label>
                            <small id="managerhelp" class="form-text text-muted">Overseas and manages the project <a href="#" class="float-right main-color">select all</a></small>
                            <select class="custom-select" id="manager" aria-describedby="managerhelp">
                                <option selected>Select Manager</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="user">user</label>
                            <small id="userhelp" class="form-text text-muted">Works on the project, will not see other users (most common) <a href="#" class="float-right main-color">select all</a></small>
                            <select class="custom-select" id="user" aria-describedby="userhelp">
                                <option selected>Select User</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="viewer">viewer</label>
                            <small id="viewerhelp" class="form-text text-muted">Can view team reports for this report <a href="#" class="float-right main-color">select all</a></small>
                            <select class="custom-select" id="viewer" aria-describedby="viewerhelp">
                                <option selected>Select Viewer</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer padding-20">
                    <button type="button" class="btn cancel-btn mb-3" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn linear-btn linear-btn-shadow mb-3">Save </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit Project Modal -->
    <div class="modal fade" id="editprojectModal" tabindex="-1" role="dialog" aria-labelledby="editprojectModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header padding-20">
                    <div class="float-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="editprojectModalTitle">edit Project</h5>
                </div>
                <div class="modal-body padding-20">
                    <form action="">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input required type="text" class="form-control" id="name" aria-describedby="namehelp" placeholder="Accounting">
                        </div>
                        <div class="form-group">
                            <label for="description">description</label>
                            <textarea required type="text" class="form-control" id="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="client">client</label>
                            <select class="custom-select">
                                <option selected>Select Client</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer padding-20">
                    <button type="button" class="btn cancel-btn mb-3" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn linear-btn linear-btn-shadow mb-3">Save </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Transfer Project Modal -->
    <div class="modal fade" id="transferprojectModal" tabindex="-1" role="dialog" aria-labelledby="transferprojectModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header padding-20">
                    <div class="float-right">
                        <button type="button" style="opacity: 0.14;" class="close float-none" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <h5 class="text-capitalize d-block" id="editprojectModalTitle">Transfer Project Accounting</h5>
                </div>
                <div class="modal-body padding-20">
                    <form action="">
                        <div class="form-group">
                            <label for="client">Destination Organization</label>
                            <select class="custom-select">
                                <option selected>Select Destination</option>
                                <option value="1">ABC</option>
                                <option value="2">XYZ</option>
                            </select>
                        </div>
                        <div class="info-text padding-10">
                            <p class="color-grey"> <span class="mr-2"><i class="fas fa-question-circle"></i></span> All project data will be preserved</p>
                            <p class="color-grey"><span class="mr-2"><i class="fas fa-question-circle"></i></span> You can only transfer projects to organizations you own.</p>
                            <p class="color-grey"><span class="mr-2"><i class="fas fa-question-circle"></i></span> Any project member who don’t belong to the destination organization will be added to that organization.</p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer padding-20">
                    <button type="button" class="btn cancel-btn mb-3" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn linear-btn linear-btn-shadow mb-3">Save </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>