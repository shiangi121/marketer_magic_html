<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/planupgrade.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <title>Marketer Magic</title>

</head>

<body>
    <header id="landing-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <a class="" href="#">
                        <img src="images/full-logo-black.svg" class="img-fluid" alt="">
                    </a>
                </div>
                <div class="col-md-6 text-right">
                    <button class="member-login text-capitalize">
                        member login
                    </button>
                </div>
            </div>
        </div>
    </header>
    <section class="header-line padding-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="mb-0 color-white"> <span class="font-weight-700">MarketerMagic</span> - Taking Your Business to a New Dimension of Marketing</p>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="landing-form my-4">
                        <div class="form-group">
                            <div class="progress">
                                <div class="progress-bar w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Order Complete</div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <img src="images/success.svg" class="img-fluid" alt="">
                        </div>
                        <div class="form-group text-center py-3 border-bottom">
                            <h2 class="font-weight-700">Thank you for your order</h2>
                            <h5 class="font-weight-600 main-color">Order number #02949202999</h5>
                        </div>

                        <div class="px-5">
                            <p class="my-4">You will receive an order confirmation email and will be redirected you to the dashboard page in
                                <span class="main-color">5 seconds…</span>
                            </p>
                            <button type="button" class="btn grediant-btn w-100" onclick="window.location.href='index.php'">go to dashboard</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer id="landing-footer">
        <div class="container">
            <div class="col-md-12 text-center">
                <ul>
                    <li><a href="#">Terms Of Service</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">DMNCA Policy</a></li>
                    <li><a href="#">Income Disclosure</a></li>
                    <li><a href="#">Affiliates</a></li>
                </ul>
                <p class="mb-0">Yes, This Page Was Built Using MarketerMagic. Unlike Our Competitors, We drink our own Kool-Aid. <br> MarketerMagic.com - an Etison Product - All Rights Reserved @ 2018 - 2020 <br> 4410 Adair Street, San Diego, California 92107</p>
            </div>
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>