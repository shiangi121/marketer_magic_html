<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/emilverify.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5  mt-77" id="emilverify-liveverification">
                <div class="row">
                    <div class="col-md-12 mobile-center">
                        <h5 class="font-weight-600 text-capitalize">live verification</h5>
                        <p class="color-grey"> <span class="mr-1" style="font-size:12px; vertical-align: middle;"><i class="fas fa-info-circle"></i></span> Follow the instructions below or ask a colleage to help.</p>
                    </div>
                </div>
                <div class="row py-3">
                    <div class="col-md-12">
                        <div class="form-box">
                            <div class="form-group">
                                <label for="copycode">1. Add this snippet to your site</label>
                                <small id="copycode" class="form-text text-muted">Add it to every page of your site that you plan to track, capture or display on. (In most cases this will be every page of your site)</small>
                                <textarea class="form-control" id="copycode" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-3">
                    <div class="col-md-12">
                        <div class="form-box">
                            <div class="form-group mb-5">
                                <label>2. Wait for data from your site</label>
                                <small class="form-text text-muted">A status message will appear below to let you know when we have detected your pixel.</small>
                                <button class="btn linear-btn d-inline" type="button">test my pixel</button>
                                <input type="text" class="form-control d-inline" disabled placeholder="Your pixel is not yet sending data">
                            </div>
                            <div class="form-group">
                                <label>3. Still no data?</label>
                                <small class="form-text text-muted">To make sure you installed your pixel correctly please watch the <a href="#" class="main-color">pixel installation video</a> and read the installation article. Here are some <a href="#" class="main-color">common things to check</a>. Feel free to send us an email at <a href="#" class="main-color">support@marketermagics.com</a> or reach out in the chat if you're still running into trouble.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>