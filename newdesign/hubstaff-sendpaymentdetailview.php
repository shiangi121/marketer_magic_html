<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/hubstaff.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="hubstaffpayment">
                <div class="row">
                    <div class="col-md-4 mobile-center">
                        <h5 class="font-weight-600">Send Payments</h5>
                        <div class="showselection d-inline">
                            <span class="color-grey">Showing</span>
                            <select class="form-control form-control-sm" onchange="location = this.value;">
                                <option selected value="hubstaff-sendpayment.php">Pay For Hours </option>
                                <option value="hubstaff-timesheet.php">Approved Timesheets</option>
                                <option value="hubstaff-onetimeamount.php">One-time Amount</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row filters">
                    <div class="col-md-6 col-lg-9">
                        <form action="" class="">
                            <div class="form-group">
                                <label for=""></label>
                                <input type="date" class="form-control" id="">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 col-lg-3 text-right mobile-left">
                        <div class="form-group">
                            <label for="" class="text-left w-100">report type</label>
                            <select class="custom-select" onchange="location = this.value;">
                                <option  value="hubstaff-sendpayment.php">Overview</option>
                                <option selected value="hubstaff-sendpaymentdetailview.php">Detailed</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row filters">
                    <div class="col-md-12 col-lg-9">
                        <form action="" class="">
                            <div class="form-group mx-2">
                                <label for="">project</label>
                                <select class="custom-select">
                                        <option selected>All Projects</option>
                                    </select>
                            </div>
                            <div class="form-group mx-2">
                                <label for="">member</label>
                                <select class="custom-select">
                                        <option selected>Adrian Smith</option>
                                        <option value="1">John Deo</option>
                                    </select>
                            </div>
                            <div class="form-group mx-2">
                                <label for="" class="d-block">INCLUDE  ARCHIVED PROJECTS</label>
                                <div id="radioBtn" class="btn-group ">
                                    <a class="btn btn-primary btn-sm active" data-toggle="include" data-title="Y">YES</a>
                                    <a class="btn btn-primary btn-sm notActive" data-toggle="include" data-title="N">NO</a>
                                </div>
                                <input type="hidden" name="include" id="include">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12 col-lg-3 text-right mobile-left">
                        <button type="button" class="btn linear-btn linear-btn-shadow">apply</button>
                    </div>
                </div>
                <div class="row py-5">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th scope="col" class="wd-10">date</th>
                                <th scope="col" class="wd-50">team member</th>
                                <th scope="col" class="wd-20">current rate</th>
                                <th scope="col" class="wd-10">unpaid amount</th>
                                <th scope="col" class="wd-10">unpaid hours</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">
                                    <p class="main-color mb-0">03/20/2019</p>
                                </td>
                                <td>
                                    <div class="member-info">
                                        <div class="float-left mr-3">
                                            <img src="images/userimg.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="member-name">
                                            <a href="#" class="font-weight-600 mb-0 d-inline text-capitalize">john deo</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0">No rates set </p>
                                </td>
                                <td>
                                    <p class="mb-0">N/A </p>
                                </td>
                                <td>
                                    <p class="mb-0">8:51:08 </p>
                                </td>

                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i> 
                                                </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">Edit pay rate</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td scope="row">
                                    <p class="main-color mb-0">03/20/2019</p>
                                </td>
                                <td>
                                    <div class="member-info">
                                        <div class="float-left mr-3">
                                            <img src="images/userimg.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="member-name">
                                            <a href="#" class="font-weight-600 mb-0 d-inline text-capitalize">john deo</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="mb-0">2.36 /hr</p>
                                </td>
                                <td>
                                    <p class="mb-0">$152.85</p>
                                </td>
                                <td>
                                    <p class="mb-0">64:45:40 </p>
                                </td>

                                <td>
                                    <div class="dropdown projectdropdown">
                                        <button class="btn dropdown-toggle padding-0 color-grey" type="button" id="projectedit" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i> 
                                                </button>
                                        <div class="dropdown-menu" aria-labelledby="projectedit">
                                            <a class="dropdown-item" href="#">Edit pay rate</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>