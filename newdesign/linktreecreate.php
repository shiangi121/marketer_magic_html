<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/header-footer.css">
    <link rel="stylesheet" href="css/urlshortner.css">

    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script> -->
    <title>Marketer Magic</title>

</head>

<body>
    <div class="wrapper">
        <!-- Header  -->
        <?php include 'header.html';?>
        <!-- Sidebar  -->
        <?php include 'sidebar.html';?>
        <!-- Page Content  -->
        <div id="content" class="active">
            <div class="container-fluid border-top px-5 py-5 mt-77" id="clickproof">
                <div class="row">
                    <div class="col-md-6 mobile-center">
                        <h4 class="font-weight-500">Link Tree</h4>
                        <p class="color-grey"> <span class="mr-2"><i class="fas fa-globe-americas"></i></span> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <div class="col-md-6 text-right mobile-center">
                        <button type="button" class="btn linear-btn linear-btn-shadow"> Create New Link</button>
                    </div>
                </div>
                <div class="row py-5" style="max-width:600px;margin:0 auto;">
                    <div class="col-md-12">
                        <div class="box-shadow padding-20">
                            <div class="form-group form-inline">
                                <label for="" class="w-100 justify-content-start mb-2">Link 1 </label>
                                <input class="form-control col-md-10" type="text" placeholder="https://www.youtube.com/channel/UCDPM_n1atn2ijUwHd0NNRQw">
                                <div class="btn-group col-md-2">
                                    <button type="button" class="btn"> <img src="images/close.svg" alt=""> </button>
                                    <!-- <button type="button" class="btn"> <img src="images/add.svg" alt=""> </button> -->
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="" class="w-100 justify-content-start mb-2">Link 2 </label>
                                <input class="form-control col-md-10" type="text" placeholder="https://www.youtube.com/channel/UCDPM_n1atn2ijUwHd0NNRQw">
                                <div class="btn-group col-md-2">
                                    <button type="button" class="btn"> <img src="images/close.svg" alt=""> </button>
                                    <!-- <button type="button" class="btn"> <img src="images/add.svg" alt=""> </button> -->
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label for="" class="w-100 justify-content-start mb-2">Link 3 </label>
                                <input class="form-control col-md-10" type="text" placeholder="https://www.youtube.com/channel/UCDPM_n1atn2ijUwHd0NNRQw">
                                <div class="btn-group col-md-2">
                                    <button type="button" class="btn"> <img src="images/close.svg" alt=""> </button>
                                    <button type="button" class="btn"> <img src="images/add.svg" alt=""> </button>
                                </div>
                            </div>
                            <button class="btn linear-btn linear-btn-shadow my-3 w-100" type="button">Create Link Tree</button>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Footer  -->
            <?php include 'footer.html';?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>