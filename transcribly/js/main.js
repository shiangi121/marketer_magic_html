$(document).ready(function() {

$('#header').load('header.html');
$('#sidebar').load('sidebar.html');
$('#footer').load('footer.html');



    $('#preview').on('click', function() {
        $('.RecentActivityPopup').addClass("d-block");
    });

    $('#close').on('click', function() {
        $('.RecentActivityPopup').removeClass("d-block");
    });


    $('#preview1').on('click', function() {
        $('.LiveVisitorPopup').addClass("d-block");
    });

    $('#close1').on('click', function() {
        $('.LiveVisitorPopup').removeClass("d-block");
    });


    $('#preview2').on('click', function() {
        $('.CustomPopup').addClass("d-block");
    });

    $('#close2').on('click', function() {
        $('.CustomPopup').removeClass("d-block");
    });

    $('#editme-button').on('click', function() {
        $('.editme-input').addClass("d-block");
        $('.editme-text').addClass("d-none");
    });

    $('#editname-button').on('click', function() {
        $('.editname-input').addClass("d-inline");
        $('.edit-name').addClass("d-none");
    });


    $('#sidebar').hover(
        function(){ 
            $(this).removeClass('active') 
        },
        function(){ 
            $(this).addClass('active') 
        }
    );

    $( "#content" ).click(function() {
        $( "#sidebar" ).addClass("active");
    });

    $("#sidebar").hover(
        function() {$('#content').removeClass("active"); },
        function() {$('#content').addClass("active"); }
    );

    $('#radioBtn a').on('click', function() {
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);

        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    });

    function DropList() {
        var n = document.getElementById("sel").options.length;
        document.getElementById("sel").size = n;
    }

    $('.btn-number').click(function(e) {
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function() {
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});